﻿using System.Collections.Generic;

namespace GameBrain
{
    public class InitPlacingDTO
    {
        public int UnitSize { get; set; }
        public List<int[]> Coordinates { get; set; } = new List<int[]>();
        public bool IsComplete { get; set; } = false;
    }
}