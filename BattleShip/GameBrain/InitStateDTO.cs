﻿using System.Collections.Generic;

namespace GameBrain
{
    public class InitStateDTO
    {
        public string Name { get; set; } = default!;
        public int Count { get; set; }
        public List<string> Placings { get; set; } = new List<string>();
        public bool IsComplete { get; set; }
    }
}