﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameBrain
{
    public class InitState
    {
        private string name;
        private int count;
        private List<InitPlacing> placings = new List<InitPlacing>();
        private bool isComplete;


        public InitState(string name, int count, int shipTypeSize)
        {
            this.name = name;
            this.count = count;
            for (int i = 0; i < count; i++)
            {
                placings.Add(new InitPlacing(shipTypeSize));
            }

        }

        public void AddCoordinates(int[] xy)
        {
            foreach (var placing in placings)
            {
                if (!placing.IsShipComplete)
                {
                    placing.AddCoordinate(xy);
                    if (count == placings.Count(p => p.IsShipComplete))
                    {
                        isComplete = true;
                        break;
                    }
                }
            }
        }

        public void AddWholeShip(int[] xy, string direction)
        {
            foreach (var placing in placings)
            {
                if (!placing.IsShipComplete)
                {
                    var x = xy[0];
                    var y = xy[1];
                    var size = placing.GetShipSize;
                    while (size > 0)
                    {
                        placing.AddCoordinate(new[] {x, y});
                        if (direction == "horizontal")
                            x++;
                        else
                            y++;

                        size--;
                    }

                    if (count == placings.Count(p => p.IsShipComplete))
                    {
                        isComplete = true;
                    }

                    break;
                }
            }
        }

        public List<InitPlacing> GetPlacings => placings;
        public string GetName => name;

        public bool HaveAllShipsPlaced
        {
            get => isComplete;
            set => isComplete = value;
        }

        public int GetCount
        {
            get => count;
            set => count = value;
        }

        public int GetCountLeft()
        {
            return count - placings.Count(p => p.IsShipComplete == true);
        }

        public string GetJsonInitState()
        {
            var dto = new InitStateDTO()
            {
                Name = name,
                Count = count,
                Placings = GetListJsonPlacings(),
                IsComplete = isComplete
            };
            return System.Text.Json.JsonSerializer.Serialize(dto);
        }

        public static InitState SetInitStateFromJson(string json)
        {
            var res = new InitState("a", 1, 1);
            var dto = System.Text.Json.JsonSerializer.Deserialize<InitStateDTO>(json);
            if (dto != null)
            {
                res.name = dto.Name;
                res.count = dto.Count;
                res.isComplete = dto.IsComplete;
                res.placings = GetListPlacings(dto.Placings);
            }

            return res;
        }

        private static List<InitPlacing> GetListPlacings(List<string> dto)
        {
            List<InitPlacing> res = new List<InitPlacing>();
            foreach (var json in dto)
            {
                res.Add(InitPlacing.SetInitPlacingFromJson(json));
            }
            return res;
        }

        private List<string> GetListJsonPlacings()
        {
            List<string> res =  new List<string>();

            foreach (var placing in placings)
            {
                res.Add(placing.GetJsonInitPlacing());
            }

            return res;
        }
    }
}