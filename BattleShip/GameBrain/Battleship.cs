﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Domain;
using Domain.Enums;

namespace GameBrain
{
    public class Battleship
    {
        private int? _gameId;
        private string? _winner;

        private TurnRule _turnRule;
        private MarginRule _shipMargin;

        private int _boardHeight;
        private int _boardWidth;

        private Dictionary<string, int>? _shipCounts;
        private Dictionary<string, int>? _shipSizes;

        private bool _nextMoveByA = true;

        private Player? _playerA;
        private Player? _playerB;

        private CellState[,]? _boardPlayerA;
        private CellState[,]? _boardPlayerB;

        private Dictionary<int, List<int[]>> _playerAShips = new Dictionary<int, List<int[]>>();
        private Dictionary<int, List<int[]>> _playerBShips = new Dictionary<int, List<int[]>>();

        private int _shipsPerPlayerOnBoard;

        private int _shipsSankByA;
        private int _shipsSankByB;

        private List<InitState> _initStates = new List<InitState>();

        public Battleship(int boardheight, int boardwidth, Dictionary<string, int> sizes,
            Dictionary<string, int> counts, TurnRule turnRule, MarginRule shipMargin)
        {
            _boardHeight = boardheight;
            _boardWidth = boardwidth;
            _shipSizes = sizes;
            _shipCounts = counts;
            _boardPlayerA = new CellState[_boardWidth, _boardHeight];
            _boardPlayerB = new CellState[_boardWidth, _boardHeight];
            _turnRule = turnRule;
            _shipMargin = shipMargin;

            var shipsPerPlayerOnBoardTemp = 0;
            foreach (var key in _shipCounts.Keys)
            {
                if (_shipSizes[key] != 0)
                {
                    shipsPerPlayerOnBoardTemp += _shipCounts[key];
                }
            }

            _shipsPerPlayerOnBoard = shipsPerPlayerOnBoardTemp;
        }

        public int? GetGameId
        {
            get => _gameId;
            set => _gameId = value;
        }

        public bool NextMoveByPlayerA => _nextMoveByA;

        public CellState[,] GetBoardForA()
        {
            var res = new CellState[_boardWidth, _boardHeight];
            Array.Copy(_boardPlayerA!, res, _boardPlayerA!.Length);
            return res;
        }

        public CellState[,] GetBoardForB()
        {
            var res = new CellState[_boardWidth, _boardHeight];
            Array.Copy(_boardPlayerB!, res, _boardPlayerB!.Length);
            return res;
        }

        public CellState GetCellFromBoard(CellState[,] board, int[] cell)
        {
            return board[cell[0], cell[1]];
        }

        public Player? GetPlayerA
        {
            get => _playerA;
            set => _playerA = value;
        }

        public Player? GetPlayerB
        {
            get => _playerB;
            set => _playerB = value;
        }

        public int GetBoardWidth
        {
            get => _boardWidth;
            set => _boardWidth = value;
        }

        public int GetBoardHeight
        {
            get => _boardHeight;
            set => _boardHeight = value;
        }

        public MarginRule GetShipMargin
        {
            get => _shipMargin;
            set => _shipMargin = value;
        }

        public TurnRule GetTurnRule
        {
            get => _turnRule;
            set => _turnRule = value;
        }

        public Dictionary<string, int>? GetShipSizes
        {
            get => _shipSizes;
            set => _shipSizes = value;
        }

        public Dictionary<string, int>? GetShipCounts
        {
            get => _shipCounts;
            set => _shipCounts = value;
        }

        public string? GetWinner
        {
            get => _winner;
            set => _winner = value;
        }

        public CellState[,]? GetBoardPlayerA
        {
            get => _boardPlayerA;
            set => _boardPlayerA = value;
        }

        public CellState[,]? GetBoardPlayerB
        {
            get => _boardPlayerB;
            set => _boardPlayerB = value;
        }

        public Dictionary<int, List<int[]>> GetPlayerAShips
        {
            get => _playerAShips;
            set => _playerAShips = value;
        }

        public Dictionary<int, List<int[]>> GetPlayerBShips
        {
            get => _playerBShips;
            set => _playerBShips = value;
        }

        public int GetShipsSankByA
        {
            get => _shipsSankByA;
            set => _shipsSankByA = value;
        }

        public int GetShipsSankByB
        {
            get => _shipsSankByB;
            set => _shipsSankByB = value;
        }

        public int GetShipsPerPlayerOnBoard
        {
            get => _shipsPerPlayerOnBoard;
            set => _shipsPerPlayerOnBoard = value;
        }

        public List<InitState> GetInitStates
        {
            get => _initStates;
            set => _initStates = value;
        }


        public List<StatHelper> GetStats(Player player)
        {
            List<StatHelper> shipStats = new List<StatHelper>();
            foreach (var ship in player.Role == PlayerRole.A ? _playerAShips.Values : _playerBShips.Values)
            {
                var size = ship.Count;
                var notSank = ship.Count(s => s != null) > 0;

                if (shipStats.Count(s => s.GetSize == size) == 0)
                {
                    var n = new StatHelper(size);
                    n.GetInitialCount++;
                    if (notSank)
                    {
                        n.GetCountLeft++;
                    }

                    shipStats.Add(n);
                }
                else
                {
                    var n = shipStats.FirstOrDefault(s => s.GetSize == size);
                    n!.GetInitialCount++;
                    if (notSank)
                    {
                        n!.GetCountLeft++;
                    }
                }
            }

            return shipStats;
        }


        public List<InitState> CreateInitState(Dictionary<string, int> sizes, Dictionary<string, int> counts)
        {
            List<InitState> result = new List<InitState>();
            List<string> types = sizes.Keys.ToList();
            foreach (var type in types)
            {
                result.Add(new InitState(type, counts[type], sizes[type]));
            }

            return result;
        }

        

        public string ReDrawPlayerBoardForWebAppShipPlacing(int[] xy, string shipTypeToSave, string direction)
        {
            _playerA = new Player("", PlayerRole.A, PlayerType.Human);

            int dictKey = _playerAShips.Keys.ToList().Count + 1;
            int size = _initStates.Last(s => s.GetName == shipTypeToSave).GetPlacings[0].GetShipSize;

            var res = MarkShipToGameBoard(xy, direction, _playerA, size, dictKey);
            if (res == "")
            {
                var state = _initStates.Last(s => s.GetName == shipTypeToSave);
                state.AddWholeShip(xy, direction);
            }

            return res;
        }

        public string MarkShipToGameBoard(int[] xy, string direction, Player player, int size, int dictKey)
        {
            int x = xy[0];
            int y = xy[1];
            var res = CheckPositionsToMarkShip(x, y, player.Role == PlayerRole.A ? _boardPlayerA : _boardPlayerB,
                direction, size);
            if (!res.Equals(""))
                return res;
            MarkCheckedPositions(x, y, player, direction, size, dictKey);
            return "";
        }

        private void MarkCheckedPositions(int xPr, int yPr, Player player, string direction, int sizePr, int key)
        {
            var shipSlots = new List<int[]>();
            var board = player.Role == PlayerRole.A ? _boardPlayerA : _boardPlayerB;
            var playersShips = player.Role == PlayerRole.A ? _playerAShips : _playerBShips;

            while (sizePr > 0)
            {
                board![xPr, yPr] = CellState.Ship;
                shipSlots.Add(new[] {xPr, yPr});
                if (direction == "horizontal")
                    xPr++;
                else
                    yPr++;

                sizePr--;
            }

            playersShips.Add(key, shipSlots);
        }

        private String CheckPositionsToMarkShip(int xPr, int yPr, CellState[,]? board, string direction, int sizePr)
        {
            var x = xPr;
            var y = yPr;
            var size = sizePr;

            try
            {
                while (size > 0)
                {
                    if (board![x, y] == CellState.Ship)
                    {
                        return "Ships cannot overlay or cross each other";
                    }

                    if (direction == "horizontal")
                        x++;
                    else
                        y++;

                    size--;
                }
            }
            catch (IndexOutOfRangeException)
            {
                return "Cannot fit this ship into the board from this location.";
            }

            var x0 = xPr;
            var y0 = yPr;
            var xm1 = xPr - 1;
            var ym1 = yPr - 1;
            var xp1 = xPr + 1;
            var yp1 = yPr + 1;

            size = sizePr;
            x = xPr;
            y = yPr;
            List<int[]> horizontalEdgesChecks = new List<int[]>
            {
                new[] {x - 1, y},
                new[] {x + size, y}
            };
            List<int[]> verticalEdgesChecks = new List<int[]>
            {
                new[] {x, y - 1},
                new[] {x, y + size}
            };

            if (_shipMargin != MarginRule.CanTouchSides)
            {
                if (direction == "horizontal")
                    if (!CheckPositionsFromList(horizontalEdgesChecks, board!))
                        return "Current game configuration does not allow ships to touch by sides.";
                if (direction == "vertical")
                    if (!CheckPositionsFromList(verticalEdgesChecks, board!))
                        return "Current game configuration does not allow ships to touch by sides.";

                if (direction == "horizontal")
                    while (size > 0)
                    {
                        try
                        {
                            if (board![x0, ym1] == CellState.Ship)
                            {
                                return "Current game configuration does not allow ships to touch by sides.";
                            }
                        }
                        catch (IndexOutOfRangeException)
                        {
                            //ignored
                        }

                        try
                        {
                            if (board![x0, yp1] == CellState.Ship)
                            {
                                return "Current game configuration does not allow ships to touch by sides.";
                            }
                        }
                        catch (IndexOutOfRangeException)
                        {
                            //ignored
                        }

                        size--;
                        x0++;
                    }

                size = sizePr;
                if (direction == "vertical")
                    while (size > 0)
                    {
                        try
                        {
                            if (board![xm1, y0] == CellState.Ship)
                            {
                                return "Current game configuration does not allow ships to touch by sides.";
                            }
                        }
                        catch (IndexOutOfRangeException)
                        {
                            //ignored
                        }

                        try
                        {
                            if (board![xp1, y0] == CellState.Ship)
                            {
                                return "Current game configuration does not allow ships to touch by sides.";
                            }
                        }
                        catch (IndexOutOfRangeException)
                        {
                            //ignored
                        }

                        size--;
                        y0++;
                    }
            }

            x = xPr;
            y = yPr;
            size = sizePr;
            List<int[]> horizontalCornersChecks = new List<int[]>
            {
                new[] {x - 1, y - 1},
                new[] {x - 1, y + 1},
                new[] {x + size, y - 1},
                new[] {x + size, y + 1}
            };
            List<int[]> verticalCornersChecks = new List<int[]>
            {
                new[] {x - 1, y - 1},
                new[] {x + 1, y - 1},
                new[] {x - 1, y + size},
                new[] {x + 1, y + size}
            };

            if (_shipMargin == MarginRule.CannotTouch)
            {
                if (direction == "horizontal")
                    if (!CheckPositionsFromList(horizontalCornersChecks, board!))
                        return "Current game configuration does not allow ships to touch by corners";

                if (direction == "vertical")
                    if (!CheckPositionsFromList(verticalCornersChecks, board!))
                        return "Current game configuration does not allow ships to touch by corners";
            }

            return "";
        }

        public static bool CheckPositionsFromList(List<int[]> list, CellState[,] board)
        {
            foreach (var cell in list)
                try
                {
                    if (board[cell[0], cell[1]] == CellState.Ship)
                    {
                        return false;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    //ignored
                }

            return true;
        }

        

        public MoveReturn MakeAMove(int[] xy, Player player)
        {
            int x = xy[0];
            int y = xy[1];
            var targetBoard = player.Role == PlayerRole.B ? _boardPlayerA : _boardPlayerB;

            if (targetBoard![x, y] == CellState.Free)
            {
                targetBoard![x, y] = CellState.Miss;
                _nextMoveByA = !_nextMoveByA;
                return MoveReturn.Miss;
            }

            if (targetBoard![x, y] == CellState.Ship)
            {
                targetBoard![x, y] = CellState.Hit;

                if (CheckIfSunk(player.Role == PlayerRole.A ? _playerB : _playerA, xy))
                {
                    if (_turnRule == TurnRule.CanHaveTurnsInRow)
                    {
                        return MoveReturn.HitAndSankRetry;
                    }

                    _nextMoveByA = !_nextMoveByA;
                    return MoveReturn.HitAndSankNoRetry;
                }

                if (_turnRule == TurnRule.CanHaveTurnsInRow)
                {
                    return MoveReturn.HitRetry;
                }

                _nextMoveByA = !_nextMoveByA;
                return MoveReturn.HitNoRetry;
            }

            if (targetBoard[x, y] == CellState.Hit || targetBoard[x, y] == CellState.Miss)
            {
                return MoveReturn.SameSpotTwice;
            }

            return MoveReturn.Null;
        }

        public bool CheckIfSunk(Player? player, int[] cell)
        {
            var isSank = false;
            if (player!.Role == PlayerRole.A)
            {
                foreach (var ship in _playerAShips.Values)
                foreach (var loc in ship)
                    if (loc != null)
                    {
                        if (loc[0] == cell[0] && loc[1] == cell[1])
                        {
                            ship[ship.IndexOf(loc)] = null!;
                            var count = ship.Count(s => s != null);
                            if (count == 0)
                            {
                                isSank = true;
                                _shipsSankByB++;
                                CheckForWin(player);
                            }

                            break;
                        }
                    }
            }
            else
            {
                foreach (var ship in _playerBShips.Values)
                foreach (var loc in ship)
                    if (loc != null)
                    {
                        if (loc[0] == cell[0] && loc[1] == cell[1])
                        {
                            ship[ship.IndexOf(loc)] = null!;
                            var count = ship.Count(s => s != null);
                            if (count == 0)
                            {
                                isSank = true;
                                _shipsSankByA++;
                                CheckForWin(player);
                            }

                            break;
                        }
                    }
            }
            return isSank;
        }

        public void CheckForWin(Player player)
        {
            if (_shipsSankByA == _shipsPerPlayerOnBoard || _shipsSankByB == _shipsPerPlayerOnBoard)
            {
                if (player.Role == PlayerRole.A)
                {
                    _winner = _playerB!.Name;
                }
                else
                {
                    _winner = _playerA!.Name;
                }
            }
        }

        
        

        public string GetSerializedGameState()
        {
            var state = new GameState
            {
                PlayerAName = _playerA!.Name,
                PlayerARole = _playerA!.Role,
                PlayerAType = _playerA!.Type,
                PlayerBName = _playerB!.Name,
                PlayerBRole = _playerB!.Role,
                PlayerBType = _playerB!.Type,
                NextMoveByA = NextMoveByPlayerA,
                Width = _boardWidth,
                Height = _boardHeight,
                SankByA = _shipsSankByA,
                SankByB = _shipsSankByB,
                ShipsPlaced = _shipsPerPlayerOnBoard,
                ShipSizes = _shipSizes,
                ShipCounts = _shipCounts,
                PlayerAFleet = _playerAShips,
                PlayerBFleet = _playerBShips,
                MayHaveTurnsInRow = _turnRule,
                ShipMargin = _shipMargin
            };
            state.BoardA = new CellState[state.Width][];
            for (var i = 0; i < state.BoardA.Length; i++) state.BoardA[i] = new CellState[state.Height];

            for (var x = 0; x < state.Width; x++)
            for (var y = 0; y < state.Height; y++)
                state.BoardA[x][y] = _boardPlayerA![x, y];

            state.BoardB = new CellState[state.Width][];
            for (var i = 0; i < state.BoardB.Length; i++) state.BoardB[i] = new CellState[state.Height];

            for (var x = 0; x < state.Width; x++)
            for (var y = 0; y < state.Height; y++)
                state.BoardB[x][y] = _boardPlayerB![x, y];

            var jsonOption = new JsonSerializerOptions
            {
                WriteIndented = true
            };

            return JsonSerializer.Serialize(state, jsonOption);
        }

        public void SetGameStateFromJsonString(string jsonString)
        {
            var state = JsonSerializer.Deserialize<GameState>(jsonString);

            _nextMoveByA = state!.NextMoveByA;

            _boardPlayerA = new CellState[state!.Width, state.Height];
            _boardPlayerB = new CellState[state!.Width, state.Height];
            for (var x = 0; x < state.Width; x++)
            for (var y = 0; y < state.Height; y++)
                _boardPlayerA[x, y] = state.BoardA[x][y];
            for (var x = 0; x < state.Width; x++)
            for (var y = 0; y < state.Height; y++)
                _boardPlayerB[x, y] = state.BoardB[x][y];

            _playerA = new Player(state.PlayerAName!, state.PlayerARole, state.PlayerAType);
            _playerB = new Player(state.PlayerBName!, state.PlayerBRole, state.PlayerBType);
            _boardWidth = state.Width;
            _boardHeight = state.Height;
            _shipsSankByA = state.SankByA;
            _shipsSankByB = state.SankByB;
            _shipsPerPlayerOnBoard = state.ShipsPlaced;
            _shipSizes = state.ShipSizes;
            _shipCounts = state.ShipCounts;
            _playerAShips = state.PlayerAFleet!;
            _playerBShips = state.PlayerBFleet!;
            _turnRule = state.MayHaveTurnsInRow;
            _shipMargin = state.ShipMargin;
        }


        public static string GetSerializedDictionary<T, V>(Dictionary<T, V>? input)
        {
            var jsonOption = new JsonSerializerOptions
            {
                WriteIndented = true
            };

            return JsonSerializer.Serialize(input, jsonOption);
        }

        public static string GetSerializedBoard(CellState[,] input)
        {
            var width = input.GetUpperBound(0) + 1;
            var height = input.GetUpperBound(1) + 1;
            var board = new CellState[width][];
            for (var i = 0; i < board.Length; i++) board[i] = new CellState[height];

            for (var x = 0; x < width; x++)
            for (var y = 0; y < height; y++)
                board[x][y] = input[x, y];

            var jsonOption = new JsonSerializerOptions
            {
                WriteIndented = true
            };

            return JsonSerializer.Serialize(board, jsonOption);
        }

        public void SetGameStateFromDb(Board b)
        {
            if (b.Winner != null)
            {
                _winner = b.Winner;
            }

            _gameId = b.GameId;
            _turnRule = b.Game!.GameOption!.TurnRule;
            _shipMargin = b.Game!.GameOption!.MarginRule;
            _boardHeight = b.Game!.GameOption!.BoardHeight;
            _boardWidth = b.Game!.GameOption!.BoardWidth;
            _shipCounts = GetDeserializedDictionary<string, int>(b.Game!.GameOption!.ShipCounts);
            _shipSizes = GetDeserializedDictionary<string, int>(b.Game!.GameOption!.ShipSizes);
            _nextMoveByA = b.NextTurnByWho;
            _playerA = new Player(b.Game.PlayerA!.Name!, b.Game.PlayerA.PlayerRole, b.Game.PlayerA.PlayerType);
            _playerB = new Player(b.Game.PlayerB!.Name!, b.Game.PlayerB.PlayerRole, b.Game.PlayerB.PlayerType);
            _boardPlayerA = GetDeserializedBoard(b.PlayerABoard);
            _boardPlayerB = GetDeserializedBoard(b.PlayerBBoard);
            _playerAShips = GetDeserializedDictionary<int, List<int[]>>(b.PlayerAShips)!;
            _playerBShips = GetDeserializedDictionary<int, List<int[]>>(b.PlayerBShips)!;
            var shipsPerPlayerOnBoardTemp = 0;
            foreach (var key in _shipCounts!.Keys)
            {
                if (_shipSizes![key] != 0)
                {
                    shipsPerPlayerOnBoardTemp += _shipCounts[key];
                }
            }

            _shipsPerPlayerOnBoard = shipsPerPlayerOnBoardTemp;
            _shipsSankByA = b.ShipsSankByA;
            _shipsSankByB = b.ShipsSankByB;
        }

        public CellState[,] GetDeserializedBoard(string? s)
        {
            var state = JsonSerializer.Deserialize<CellState[][]>(s!);

            var board = new CellState[_boardWidth, _boardHeight];
            for (var x = 0; x < _boardWidth; x++)
            for (var y = 0; y < _boardHeight; y++)
                board[x, y] = state![x][y];

            return board;
        }

        public static Dictionary<T, V>? GetDeserializedDictionary<T, V>(string? s)
        {
            var state = JsonSerializer.Deserialize<Dictionary<T, V>>(s!);

            return state;
        }


        public string GetSerializedInitStatesList()
        {
            List<string> res = new List<string>();
            foreach (var state in _initStates)
            {
                res.Add(state.GetJsonInitState());
            }

            return JsonSerializer.Serialize(res);
        }

        public List<InitState> GetDeserializedInitStatesList(string jsons)
        {
            var strings = JsonSerializer.Deserialize<List<string>>(jsons);
            List<InitState> res = new List<InitState>();
            if (strings != null)
            {
                foreach (var json in strings)
                {
                    res.Add(InitState.SetInitStateFromJson(json));
                }
            }

            return res;
        }


        public static int CheckGameConfiguration(int boardWidth, int boardHeight, Dictionary<string, int> shipCounts,
            Dictionary<string, int> shipSizes, MarginRule margin)
        {

            var shipSizesList = shipSizes!.Values.ToList();
            var shipCountsList = shipCounts!.Values.ToList();

            var maxShipSize = shipSizesList.Max();

            var availableGameSlots = boardHeight * boardWidth;

            int shipSlots = 0;

            for (int i = 0; i < 5; i++)
            {
                var tmp = 0;
                if (margin == MarginRule.CannotTouch)
                {
                    if (shipSizesList[i] != 0)
                    {
                        tmp = shipSizesList[i] * 3 + 6;
                    }
                }
                else if (margin == MarginRule.CanTouchCorners)
                {
                    if (shipSizesList[i] != 0)
                    {
                        tmp = shipSizesList[i] * 3 + 2;
                    }
                }
                else if (margin == MarginRule.CanTouchSides)
                {
                    if (shipSizesList[i] != 0)
                    {
                        tmp = shipSizesList[i];
                    }
                }

                tmp *= shipCountsList[i];
                shipSlots += tmp;
            }

            var neededRoomForShips = (int) (1.1 * shipSlots);
            
            if (neededRoomForShips > availableGameSlots)
            {
                var res = (int) Math.Sqrt(neededRoomForShips) + 1;
                if (res < maxShipSize)
                {
                    res = maxShipSize;
                }

                return res;
            }

            return 0;
        }
    }
}