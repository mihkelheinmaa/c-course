﻿namespace GameBrain
{
    public class Player
    {
        public string Name;
        public PlayerRole Role;
        public PlayerType Type;

        public Player(string name, PlayerRole role, PlayerType type)
        {
            Name = name;
            Role = role;
            Type = type;
        }
    }
}