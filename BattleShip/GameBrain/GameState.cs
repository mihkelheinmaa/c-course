﻿using System.Collections.Generic;
using Domain.Enums;

namespace GameBrain
{
    public class GameState
    {
        public Player Winner { get; set; } = null!;
        public bool NextMoveByA { get; set; }
        public CellState[][] BoardA { get; set; } = null!;
        public CellState[][] BoardB { get; set; } = null!;
        public int Width { get; set; }
        public int Height { get; set; }

        public int SankByA { get; set; }
        public int SankByB { get; set; }

        public int ShipsPlaced { get; set; }

        public Dictionary<string, int>? ShipSizes { get; set; } 
        public Dictionary<string, int>? ShipCounts { get; set; }

        public Dictionary<int, List<int[]>>? PlayerAFleet { get; set; }
        public Dictionary<int, List<int[]>>? PlayerBFleet { get; set; }
        public TurnRule MayHaveTurnsInRow { get; set; }
        public MarginRule ShipMargin { get; set; }

        public string? PlayerAName { get; set; }
        public PlayerRole PlayerARole { get; set; }
        public PlayerType PlayerAType { get; set; }
        public string? PlayerBName { get; set; }
        public PlayerRole PlayerBRole { get; set; }
        public PlayerType PlayerBType { get; set; }
    }
}