﻿using System.Collections.Generic;
using System.Linq;

namespace GameBrain
{
    public class InitPlacing
    {

        private int unitSize;
        private List<int[]> coordinates = new List<int[]>();
        private bool isComplete = false;

        public InitPlacing(int shipSize)
        {
            unitSize = shipSize;
        }

        public bool AddCoordinate(int[] xy)
        {
            coordinates.Add(xy);

            if (unitSize == coordinates.Count)
            {
                isComplete = true;
            }
            return isComplete;
        }

        public bool IsShipComplete => isComplete;
        
        public int GetShipSize => unitSize;
        
        public List<int[]> GetCoordinates => coordinates;

        public string GetJsonInitPlacing()
        {
            var dto = new InitPlacingDTO()
            {
                UnitSize = unitSize,
                Coordinates = coordinates,
                IsComplete = isComplete,
            };
            return System.Text.Json.JsonSerializer.Serialize(dto);
        }

        public static InitPlacing SetInitPlacingFromJson(string json)
        {
            var res = new InitPlacing(1);
            var dto = System.Text.Json.JsonSerializer.Deserialize<InitPlacingDTO>(json);
            if (dto != null)
            {
                res.unitSize = dto.UnitSize;
                res.coordinates = dto.Coordinates;
                res.isComplete = dto.IsComplete;
            }
            return res;
        }
        
    }
    
}