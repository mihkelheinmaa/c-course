﻿using System;
using System.Collections.Generic;
using Domain.Enums;

namespace GameBrain
{
    public class AI
    {
        public static void PlaceShips(Battleship game, string role)
        {
            List<int[]> availableSlots = new List<int[]>();
            for (int i = 0; i < game.GetBoardWidth; i++)
            {
                for (int j = 0; j < game.GetBoardHeight; j++)
                {
                    availableSlots.Add(new []{i, j});
                }
            }

            foreach (var key in game.GetShipCounts!.Keys)
            {
                var count = game.GetShipCounts![key];
                var size = game.GetShipSizes![key];
                while (count > 0)
                {
                    var dir = "horizontal";
                    var dirNr = new Random().Next(0, 2);
                    if (dirNr == 1)
                    {
                        dir = "vertical";
                    }
                    var slotToTry = new Random().Next(0, availableSlots.Count);

                    var coordinates = availableSlots[slotToTry];
                    availableSlots.RemoveAt(slotToTry);
                    var res = "";
                    if (role.Equals("A"))
                    {
                        res = game.MarkShipToGameBoard(coordinates, dir, game.GetPlayerA!, size, game.GetPlayerAShips.Keys.Count + 1);
                    } else if (role.Equals("B"))
                    {
                        res = game.MarkShipToGameBoard(coordinates, dir, game.GetPlayerB!, size, game.GetPlayerBShips.Keys.Count + 1);
                    }
                    
                    if (res.Equals(""))
                    {
                        count--;
                    }
                }
            }
        }

        public static void MakeAMove(Battleship game, Player player)
        {
            CellState[,] board = (player.Role == PlayerRole.A ? game.GetBoardPlayerB : game.GetBoardPlayerA)!;
            List<int[]> availableSlots = new List<int[]>();
            List<int[]> hitSlots = new List<int[]>();
            for (int i = 0; i < game.GetBoardWidth; i++)
            {
                for (int j = 0; j < game.GetBoardHeight; j++)
                {
                    if (board[i,j] == CellState.Free || board[i,j] == CellState.Ship)
                    {
                        availableSlots.Add(new []{i, j});
                    }

                    if (board[i,j] == CellState.Hit)
                    {
                        try
                        {
                            if (board[i + 1, j] == CellState.Free || board[i + 1, j] == CellState.Ship)
                            {
                                hitSlots.Add(new []{i + 1, j});
                            }
                        }
                        catch (Exception)
                        {
                            // ignored
                        }

                        try
                        {
                            if (board[i, j - 1] == CellState.Free || board[i, j - 1] == CellState.Ship)
                            {
                                hitSlots.Add(new []{i, j - 1});
                            }
                        }
                        catch (Exception)
                        {
                            // ignored
                        }

                        try
                        {
                            if (board[i - 1, j] == CellState.Free || board[i - 1, j] == CellState.Ship)
                            {
                                hitSlots.Add(new []{i - 1, j});
                            }
                        }
                        catch (Exception)
                        {
                            // ignored
                        }

                        try
                        {
                            if (board[i, j + 1] == CellState.Free || board[i, j + 1] == CellState.Ship)
                            {
                                hitSlots.Add(new []{i, j + 1});
                            }
                        }
                        catch (Exception)
                        {
                            // ignored
                        }
                    }
                }
            }

            int[] coordinates;
            if (hitSlots.Count > 0)
            {
                coordinates = hitSlots[0];
            }
            else
            {
                var slotToTry = new Random().Next(0, availableSlots.Count);
                coordinates = availableSlots[slotToTry];
            }

            game.MakeAMove(coordinates, player);
        }
    }
}