﻿namespace GameBrain
{
    public class StatHelper
    {
        private int _size;
        private int _initialCount = 0;
        private int _countLeft = 0;
        
        public StatHelper(int size)
        {
            _size = size;
        }

        public int GetSize
        {
            get => _size;
            set => _size = value;
        }
        
        public int GetInitialCount
        {
            get => _initialCount;
            set => _initialCount = value;
        }
        
        public int GetCountLeft
        {
            get => _countLeft;
            set => _countLeft = value;
        }
    }
}