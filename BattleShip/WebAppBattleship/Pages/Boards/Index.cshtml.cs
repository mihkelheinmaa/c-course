using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;
using Player = GameBrain.Player;

namespace WebAppBattleship.Pages_Boards
{
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _context;

        public IndexModel(AppDbContext context)
        {
            _context = context;
        }

        public IList<Board> Board { get;set; } = default!;
        public string PlayerA { get; set; } = "Player A";
        public string PlayerB { get; set; } = "Player B";

        public async Task<IActionResult> OnGetAsync(int? gameId)
        {
            if (gameId == null)
            {
                return NotFound();
            }

            Board = await _context.Boards
                .Where(b => b.GameId == gameId)
                .Include(b => b.Game)
                .Include(b => b.Game!.PlayerA)
                .Include(b => b.Game!.PlayerB)
                .Include(b => b.Game!.GameOption)
                .ToListAsync();
            
            if (Board.Count < 1)
            {
                return NotFound();
            }

            PlayerA = Board[0]!.Game!.PlayerA!.Name;
            PlayerB = Board[0]!.Game!.PlayerB!.Name;
            
            return Page();
        }
        
        

    }
}
