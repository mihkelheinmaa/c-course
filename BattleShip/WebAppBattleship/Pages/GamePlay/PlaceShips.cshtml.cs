﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Domain;
using Domain.Enums;
using GameBrain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Player = Domain.Player;

namespace WebAppBattleship.Pages.GamePlay
{
    public class PlaceShips : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public PlaceShips(DAL.AppDbContext context)
        {
            _context = context;
        }

        public Game? Game { get; set; }

        public GameOption GameOption { get; set; } = default!;

        public Player? PlayerInFocus { get; set; }

        public Battleship GBattleship { get; set; } = default!;
        public InitiationState? InitiationState { get; set; }

        public List<InitiationState> InitiationStates { get; set; } = default!;

        public string NextShipName { get; set; } = default!;
        public int NextShipSize { get; set; }
        public int NextShipCountLeft { get; set; }
        public string Message { get; set; } = "";

        public string Direction { get; set; } = "horizontal";

        public async Task<IActionResult> OnGetAsync(int gameId, int playerId, int? x, int? y, string? dir, string? ship, int? initId)
        {
            Game = await _context.Games
                .Where(g => g.GameId == gameId)
                .Include(g => g.PlayerA)
                .Include(g => g.PlayerB)
                .Include(g => g.GameOption)
                .FirstOrDefaultAsync();

            PlayerInFocus = await _context.Players.Where(p => p.PlayerId == playerId).FirstOrDefaultAsync();
            
            if (PlayerInFocus == null || Game == null)
            {
                return NotFound();
            }
            
            GameOption = Game!.GameOption!;
            
            InitiationStates = await _context.InitiationStates
                .Where(s => s.GameId == gameId)
                .Where(s => s.PlayerId == playerId)
                .Include(s => s.Game)
                .Include(s => s.Player)
                .OrderBy(s => s.SavedAt)
                .ToListAsync();

            if (initId == null)
            {
                InitiationState = InitiationStates.LastOrDefault();
            } else
            {
                var i = InitiationStates.IndexOf(InitiationStates.LastOrDefault(s => s.InitiationStateId == initId)!);
                InitiationState = InitiationStates[i];
            }


            if (dir == "horizontal" || dir == "vertical")
            {
                Direction = dir;
            }

            var boardHeight = GameOption!.BoardHeight;
            var boardWidth = GameOption!.BoardWidth;
            var sizes = Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipSizes);
            var counts = Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipCounts);
            var turnRule = GameOption.TurnRule;
            var marginRule = GameOption.MarginRule;

            GBattleship = new Battleship(boardHeight, boardWidth, sizes!, counts!, turnRule, marginRule);
            GBattleship.GetPlayerA = new GameBrain.Player(PlayerInFocus!.Name!, PlayerInFocus.PlayerRole, PlayerInFocus.PlayerType);
            GBattleship.GetBoardPlayerA = new CellState[GameOption.BoardWidth, GameOption.BoardHeight];
            
            GBattleship.GetInitStates = GBattleship.CreateInitState(Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipSizes)!,
                Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipCounts)!);
            GBattleship.GetPlayerAShips = new Dictionary<int, List<int[]>>();

            
            if (InitiationState != null)
            {
                GBattleship.GetBoardPlayerA = GBattleship.GetDeserializedBoard(InitiationState.StateBoard);
                GBattleship.GetInitStates = GBattleship.GetDeserializedInitStatesList(InitiationState.Progress);
                GBattleship.GetPlayerAShips = Battleship.GetDeserializedDictionary<int, List<int[]>>(InitiationState.PlayerShips)!;
            }
            else
            {
                var initState = new InitiationState
                {
                    StateBoard = Battleship.GetSerializedBoard(GBattleship.GetBoardPlayerA!),
                    PlayerShips = Battleship.GetSerializedDictionary(GBattleship.GetPlayerAShips),
                    Progress = GBattleship.GetSerializedInitStatesList(),
                    IsComplete = false,
                    Resolute = "",
                    GameId = Game.GameId,
                    PlayerId = PlayerInFocus.PlayerId
                };
                
                _context.InitiationStates.Add(initState);
                await _context.SaveChangesAsync();
            }

            foreach (var state in GBattleship.GetInitStates)
            {
                if (state.GetCount == 0)
                {
                    state.HaveAllShipsPlaced = true;
                } else if (state.GetPlacings[0].GetShipSize == 0)
                {
                    state.GetCount = 0;
                    state.HaveAllShipsPlaced = true;
                }
            }

            
            bool isComplete = GBattleship.GetInitStates.Count(s => s.HaveAllShipsPlaced == false) == 0;
            
            if (x != null && y !=null && ship != null)
            {
                Message = GBattleship.ReDrawPlayerBoardForWebAppShipPlacing(new[] {x.Value, y.Value}, ship,
                    Direction);
                
                isComplete = GBattleship.GetInitStates.Count(s => s.HaveAllShipsPlaced == false) == 0;
                
                var initState = new InitiationState
                {
                    StateBoard = Battleship.GetSerializedBoard(GBattleship.GetBoardPlayerA!),
                    PlayerShips = Battleship.GetSerializedDictionary(GBattleship.GetPlayerAShips),
                    Progress = GBattleship.GetSerializedInitStatesList(),
                    IsComplete = isComplete,
                    Resolute = Message,
                    GameId = Game.GameId,
                    PlayerId = PlayerInFocus.PlayerId
                };
                
                _context.InitiationStates.Add(initState);
                await _context.SaveChangesAsync();
            }

            if (initId != null)
            {
                var initState = new InitiationState
                {
                    StateBoard = InitiationState!.StateBoard,
                    PlayerShips = InitiationState!.PlayerShips,
                    Progress = InitiationState!.Progress,
                    IsComplete = InitiationState!.IsComplete,
                    Resolute = Message,
                    GameId = Game.GameId,
                    PlayerId = PlayerInFocus.PlayerId
                };
                _context.InitiationStates.Add(initState!);
                await _context.SaveChangesAsync();
            }
            
            if (!isComplete)
            {
                var shipStateInFocus = GBattleship.GetInitStates
                    .Where(s => s.HaveAllShipsPlaced == false)
                    .Where(s => s.GetCount > 0)
                    .OrderBy(s => s.GetCount)
                    .FirstOrDefault();
                
                NextShipName = shipStateInFocus!.GetName;
                NextShipSize = shipStateInFocus!.GetPlacings[0].GetShipSize;
                NextShipCountLeft = shipStateInFocus.GetCountLeft();
                
                return Page();
            }

            return RedirectToPage("/GamePlay/Initiate", new { gameId = Game.GameId } );

        }
    }
}
 