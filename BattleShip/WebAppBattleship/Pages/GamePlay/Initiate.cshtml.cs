﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Enums;
using Domain;
using GameBrain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Player = Domain.Player;

namespace WebAppBattleship.Pages.GamePlay
{
    public class Initiate : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public Initiate(DAL.AppDbContext context)
        {
            _context = context;
        }

        public Game? Game { get; set; }
        public GameOption GameOption { get; set; } = default!;
        public Player PlayerA { get; set; } = default!;
        public Player PlayerB { get; set; } = default!;

        public Player PlayerInFocus { get; set; } = default!;

        public Battleship GBattleship { get; set; } = default!;

        public List<InitiationState> InitiationStates { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int gameId)
        {
            Game = await _context.Games
                .Where(g => g.GameId == gameId)
                .Include(g => g.PlayerA)
                .Include(g => g.PlayerB)
                .Include(g => g.GameOption)
                .FirstOrDefaultAsync();
            
            if (Game == null)
            {
                return NotFound();
            }
            
            GameOption = Game!.GameOption!;
            PlayerA = Game!.PlayerA!;
            PlayerB = Game!.PlayerB!;

            InitiationStates = await _context.InitiationStates
                .Where(s => s.GameId == gameId)
                .ToListAsync();
            
            var boardHeight = GameOption!.BoardHeight;
            var boardWidth = GameOption!.BoardWidth;
            var sizes = Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipSizes);
            var counts = Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipCounts);
            var turnRule = GameOption.TurnRule;
            var marginRule = GameOption.MarginRule;

            GBattleship = new Battleship(boardHeight, boardWidth, sizes!, counts!, turnRule, marginRule);
            GBattleship.GetInitStates = GBattleship.CreateInitState(Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipSizes)!,
                Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipCounts)!);

            if (!CheckIfBoardIsComplete(InitiationStates, Game.PlayerA))
            {
                PlayerInFocus = Game.PlayerA!;
                GBattleship.GetPlayerA = new GameBrain.Player(PlayerInFocus.Name, PlayerInFocus.PlayerRole, PlayerInFocus.PlayerType);
                if (PlayerInFocus.PlayerType == PlayerType.AI)
                {
                    AI.PlaceShips(GBattleship, "A");
                    
                    var initState = new InitiationState
                    {
                        StateBoard = Battleship.GetSerializedBoard(GBattleship.GetBoardPlayerA!),
                        PlayerShips = Battleship.GetSerializedDictionary(GBattleship.GetPlayerAShips),
                        Progress = GBattleship.GetSerializedInitStatesList(),
                        IsComplete = true,
                        Resolute = "",
                        GameId = Game.GameId,
                        PlayerId = PlayerInFocus.PlayerId
                    };
                    
                    _context.InitiationStates.Add(initState!);
                    await _context.SaveChangesAsync();
                    
                    
                    return RedirectToPage("./Initiate", new {gameId});
                }
            }
            else if (CheckIfBoardIsComplete(InitiationStates, Game.PlayerA) && CheckIfBoardIsComplete(InitiationStates, Game.PlayerB))
            {
                
                var lastInitStateForA = GetCompleteInitState(PlayerA);
                var lastInitStateForB = GetCompleteInitState(PlayerB);

                var board = new Board
                {
                    BoardName = DateTime.Now.ToString("yyyy-MM-dd"),
                    Description = "fromWeb",
                    NextTurnByWho = GBattleship.NextMoveByPlayerA,
                    PlayerABoard = lastInitStateForA.StateBoard,
                    PlayerBBoard = lastInitStateForB.StateBoard,
                    PlayerAShips = lastInitStateForA.PlayerShips,
                    PlayerBShips = lastInitStateForB.PlayerShips,
                    ShipsSankByA = 0,
                    ShipsSankByB = 0,
                    GameId = Game.GameId,
                    CreatedAt = DateTime.Now
                };
                
                _context.Boards.Add(board);
                await _context.SaveChangesAsync();
                
                return RedirectToPage("/GamePlay/MidScreen", new { boardId = board.BoardId, msg = MoveReturn.Start} );
            }
            else
            {
                PlayerInFocus = Game.PlayerB!;
                GBattleship.GetPlayerB = new GameBrain.Player(PlayerInFocus.Name, PlayerInFocus.PlayerRole, PlayerInFocus.PlayerType);
                if (PlayerInFocus.PlayerType == PlayerType.AI)
                {
                    AI.PlaceShips(GBattleship, "B");
                    
                    var initState = new InitiationState
                    {
                        StateBoard = Battleship.GetSerializedBoard(GBattleship.GetBoardPlayerB!),
                        PlayerShips = Battleship.GetSerializedDictionary(GBattleship.GetPlayerBShips),
                        Progress = GBattleship.GetSerializedInitStatesList(),
                        IsComplete = true,
                        Resolute = "",
                        GameId = Game.GameId,
                        PlayerId = PlayerInFocus.PlayerId
                    };
                    
                    _context.InitiationStates.Add(initState!);
                    await _context.SaveChangesAsync();
                    
                    
                    return RedirectToPage("./Initiate", new {gameId});
                }
            }

            return Page();
        }

        private InitiationState GetCompleteInitState(Player? player)
        {
            return InitiationStates.FirstOrDefault(x => x.IsComplete && x.PlayerId == player!.PlayerId)!;
        }

        public bool CheckIfBoardIsComplete(List<InitiationState> initStates, Player? player)
        {
            foreach (var state in initStates)
            {
                if (state.PlayerId == player!.PlayerId && state.IsComplete )
                {
                    return true;
                }
            }
            return false;
        }
    }
    
    
}