﻿using System.Linq;
using System.Threading.Tasks;
using Domain;
using GameBrain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebAppBattleship.Pages.GamePlay
{
    public class Boards : PageModel
    {
        private readonly DAL.AppDbContext _context;
        
        public Boards(DAL.AppDbContext context)
        {
            _context = context;
        }

        public Board? Board { get; set; }
        public GameOption? GameOption { get; set; }
        public Battleship? GBattleship { get; set; }
        
        public async Task<IActionResult> OnGetAsync(int boardId)
        {
            Board = await _context.Boards
                .Where(b => b.BoardId == boardId)
                .Include(b => b.Game)
                .Include(b => b.Game!.PlayerA)
                .Include(b => b.Game!.PlayerB)
                .Include(b => b.Game!.GameOption)
                .FirstOrDefaultAsync();

            if (Board == null)
            {
                return NotFound();
            }
            
            GameOption = Board!.Game!.GameOption;
            
            var boardHeight = GameOption!.BoardHeight;
            var boardWidth = GameOption!.BoardWidth;
            var sizes = Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipSizes);
            var counts = Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipCounts);
            var turnRule = GameOption.TurnRule;
            var marginRule = GameOption.MarginRule;
            
            GBattleship = new Battleship(boardHeight, boardWidth, sizes!, counts!, turnRule, marginRule);
            
            GBattleship.SetGameStateFromDb(Board);
            
            return Page();
        }
    }
}