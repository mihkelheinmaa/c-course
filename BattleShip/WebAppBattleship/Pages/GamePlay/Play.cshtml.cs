﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.Enums;
using GameBrain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Player = Domain.Player;

namespace WebAppBattleship.Pages.GamePlay
{
    public class Play : PageModel
    {
        private readonly DAL.AppDbContext _context;
        
        public Play(DAL.AppDbContext context)
        {
            _context = context;
        }

        public Game Game { get; set; } = default!;
        public Board? Board { get; set; }
        public Player PlayerA { get; set; } = default!;
        public Player PlayerB { get; set; } = default!;
        public GameOption GameOption { get; set; } = default!;

        public Battleship GBattleship { get; set; } = default!;
        public List<StatHelper> StatForA { get; set; } = default!;
        public List<StatHelper> StatForB { get; set; } = default!;
        
        public MoveReturn Message { get; set; } = MoveReturn.Null;

        public int BoardId { get; set; }
        
        
        public async Task<IActionResult> OnGetAsync(int boardId, int? x, int? y, bool? fromMidScreen)
        {
            Board = await _context.Boards
                .Where(b => b.BoardId == boardId)
                .Include(b => b.Game)
                .Include(b => b.Game!.PlayerA)
                .Include(b => b.Game!.PlayerB)
                .Include(b => b.Game!.GameOption)
                .FirstOrDefaultAsync();

            if (Board == null)
            {
                return NotFound();
            }

            BoardId = boardId;

            Game = Board!.Game!;
            PlayerA = Board!.Game!.PlayerA;
            PlayerB = Board!.Game!.PlayerB;
            GameOption = Board!.Game!.GameOption!;

            var boardHeight = GameOption!.BoardHeight;
            var boardWidth = GameOption!.BoardWidth;
            var sizes = Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipSizes);
            var counts = Battleship.GetDeserializedDictionary<string, int>(GameOption.ShipCounts);
            var turnRule = GameOption.TurnRule;
            var marginRule = GameOption.MarginRule;
            
            GBattleship = new Battleship(boardHeight, boardWidth, sizes!, counts!, turnRule, marginRule);
            
            GBattleship.SetGameStateFromDb(Board);

            StatForA = new List<StatHelper>(GBattleship.GetStats(GBattleship.GetPlayerA!).OrderBy(s=> s.GetSize));
            StatForB = new List<StatHelper>(GBattleship.GetStats(GBattleship.GetPlayerB!).OrderBy(s=> s.GetSize));

            if (Board.Winner != null)
            {
                return RedirectToPage("/GamePlay/Winner", new { boardId = boardId, winner = GBattleship!.GetWinner } );
            }

            if (GBattleship.GetWinner != null)
            {
                return RedirectToPage("/GamePlay/Winner", new { boardId = boardId, winner = GBattleship.GetWinner } );
            }

            var player = GBattleship.GetPlayerA;
            if (!GBattleship.NextMoveByPlayerA)
            {
                player = GBattleship.GetPlayerB;
            }

            if (player!.Type == PlayerType.AI)
            {
                AI.MakeAMove(GBattleship, player);
                var id = SaveBoardToDb();
                
                return RedirectToPage("./Play", new { boardId = id.Result } );
            }

            if (x != null && y != null)
            {
                Message = GBattleship.MakeAMove(new[] {x.Value, y.Value}, player!);

                var id = SaveBoardToDb();
                

                if (GBattleship.GetWinner != null)
                {
                    return RedirectToPage("/GamePlay/Winner", new { boardId = id.Result, winner = GBattleship.GetWinner } );
                }
                
                return RedirectToPage("/GamePlay/MidScreen", new { boardId = id.Result, msg = Message } );
                
            }
            
            return Page();
        }

        private async Task<int> SaveBoardToDb()
        {
            var board = new Board
            {
                Winner = GBattleship.GetWinner,
                BoardName = DateTime.Now.ToString("yyyy-MM-dd"),
                Description = $"FromWeb_{Board!.BoardId + 1}",
                
                NextTurnByWho = GBattleship.NextMoveByPlayerA,

                PlayerABoard = Battleship.GetSerializedBoard(GBattleship.GetBoardPlayerA!),
                PlayerBBoard = Battleship.GetSerializedBoard(GBattleship.GetBoardPlayerB!),

                PlayerAShips = Battleship.GetSerializedDictionary(GBattleship.GetPlayerAShips),
                PlayerBShips = Battleship.GetSerializedDictionary(GBattleship.GetPlayerBShips),

                ShipsSankByA = GBattleship.GetShipsSankByA,
                ShipsSankByB = GBattleship.GetShipsSankByB,

                Game = Game,
                
                CreatedAt = DateTime.Now
            };
                
            _context.Boards.Add(board);
            await _context.SaveChangesAsync();

            return board.BoardId;
        }
    }
}