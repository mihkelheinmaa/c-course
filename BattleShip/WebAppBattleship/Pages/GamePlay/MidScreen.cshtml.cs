﻿using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebAppBattleship.Pages.GamePlay
{
    public class MidScreen : PageModel
    {
        
        private readonly DAL.AppDbContext _context;
        
        public MidScreen(DAL.AppDbContext context)
        {
            _context = context;
        }
        
        public int BoardId { get; set; }
        public MoveReturn Message { get; set; }

        public Player NextTurnPlayer { get; set; } = default!;
        
        public async Task<IActionResult> OnGetAsync(int? boardId, MoveReturn? msg)
        {
            if (boardId == null || msg == null)
            {
                return NotFound();
            }

            BoardId = boardId.Value;

            var board = await _context.Boards
                .Where(b => b.BoardId == boardId)
                .Include(b => b.Game)
                .Include(b => b.Game!.PlayerA)
                .Include(b => b.Game!.PlayerB)
                .FirstOrDefaultAsync();

            if (board.NextTurnByWho)
            {
                NextTurnPlayer = board.Game!.PlayerA!;
            }
            else
            {
                NextTurnPlayer = board.Game!.PlayerB!;
            }

            Message = msg.Value;
            
            
            return Page();
            
        }
    }
}