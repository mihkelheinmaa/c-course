﻿using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebAppBattleship.Pages.GamePlay
{
    public class Winner : PageModel
    {
        
        private readonly DAL.AppDbContext _context;
        
        public Winner(DAL.AppDbContext context)
        {
            _context = context;
        }

        public Board? Board { get; set; }
        public Game Game { get; set; } = default!;
        public string? GWinner { get; set; }

        public bool NoWinner { get; set; } = false;

        public async Task<IActionResult> OnGetAsync(int boardId, string winner)
        {
            Board = await _context.Boards
                .Where(b => b.BoardId == boardId)
                .Include(b => b.Game)
                .Include(b => b.Game!.PlayerA)
                .Include(b => b.Game!.PlayerB)
                .Include(b => b.Game!.GameOption)
                .FirstOrDefaultAsync();

            if (Board == null)
            {
                return NotFound();
            }
            
            if (Board?.Winner == null)
            {
                NoWinner = true;
            }

            Game = Board!.Game!;

            GWinner = winner;
            
            return Page();
        }



    }
}