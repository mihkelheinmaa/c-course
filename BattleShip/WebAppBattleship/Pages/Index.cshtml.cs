﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace WebAppBattleship.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly DAL.AppDbContext _context;
        

        public IndexModel(DAL.AppDbContext context, ILogger<IndexModel> logger)
        {
            _logger = logger;
            _context = context;
        }

        public IList<Game> Game { get; set; } = default!;

        public async Task OnGetAsync()
        {
            Game = await _context.Games
                .Include(g => g.PlayerA)
                .Include(g => g.PlayerB)
                .Include(g => g.Boards)
                .OrderBy(g => g.GameCreatedAt)
                .ToListAsync();
        }

        public IActionResult  OnPost()
        {
            return Redirect("./NewGame/NewGame");
        }
    }
}