using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;
using Domain.Enums;
using GameBrain;
using Player = Domain.Player;

namespace WebAppBattleship.Pages_Games
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        // public IActionResult OnGet()
        // {
        //     return Page();
        // }

        [BindProperty] 
        public string PlayerA { get; set; } = default!;
        [BindProperty]
        public string PlayerB { get; set; } = default!;
        [BindProperty]
        public string? GameName { get; set; }

        [BindProperty] 
        public MarginRule MarginRule { get; set; } = MarginRule.CannotTouch;
        [BindProperty]
        public TurnRule TurnRule { get; set; }
        [BindProperty]
        public int BoardHeight { get; set; }
        [BindProperty]
        public int BoardWidth { get; set; }
        [BindProperty]
        public int CarrierSize { get; set; }
        [BindProperty]
        public int CarrierCount { get; set; }
        [BindProperty]
        public int BattleshipSize { get; set; }
        [BindProperty]
        public int BattleshipCount { get; set; }
        [BindProperty]
        public int CruiserSize { get; set; }
        [BindProperty]
        public int CruiserCount { get; set; }
        [BindProperty]
        public int SubmarineSize { get; set; }
        [BindProperty]
        public int SubmarineCount { get; set; }
        [BindProperty]
        public int DestroyerSize { get; set; }
        [BindProperty]
        public int DestroyerCount { get; set; }
        [BindProperty]
        public string GameType { get; set; } = default!;
        
        public string? Message { get; set; }
        public int SideSize { get; set; }

        public string GamePlayType { get; set; } = default!;


        public void OnGet(string? gp)
        {
            if (gp == null)
            {
                GameType = "2H";
            }
            else
            {
                GameType = gp;
            }
            
        }
        
        

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            
            var countOfShips = new Dictionary<string, int>
                {{"Carrier", CarrierCount}, {"Battleship", BattleshipCount}, {"Cruiser", CruiserCount}, {"Submarine", SubmarineCount}, {"Destroyer", DestroyerCount}};
            var shipSizes = new Dictionary<string, int>
                {{"Carrier", CarrierSize}, {"Battleship", BattleshipSize}, {"Cruiser", CruiserSize}, {"Submarine", SubmarineSize}, {"Destroyer", DestroyerSize}};

            var confCheck =
                Battleship.CheckGameConfiguration(BoardWidth, BoardHeight, countOfShips, shipSizes, MarginRule);
            if (confCheck != 0)
            {
                SideSize = confCheck;
                BoardHeight = confCheck;
                BoardWidth = confCheck;
                Message = "Your board could not hold your ships. Board size has been updated to minimum viable size. Press 'Play' to continue.";
                return Page();
            }
            
            var gameOptions = new GameOption()
            {
                BoardWidth = BoardWidth,
                BoardHeight = BoardHeight,
                MarginRule = MarginRule,
                TurnRule = TurnRule,
                ShipSizes = Battleship.GetSerializedDictionary(shipSizes),
                ShipCounts = Battleship.GetSerializedDictionary(countOfShips)
            };
            
            Player playerA;
            if (_context.Players.Any(x => x.Name!.Equals(PlayerA) && x.PlayerRole == PlayerRole.A))
            {
                playerA = _context.Players.Single(x => x.Name!.Equals(PlayerA) && x.PlayerRole == PlayerRole.A);
            }
            else
            {
                var type = PlayerType.Human;
                if (GameType == "2AI")
                {
                    type = PlayerType.AI;
                }
                playerA = new Player
                {
                    Name = PlayerA!,
                    PlayerType = type,
                    PlayerRole = PlayerRole.A,
                };
                
                
            }

            Player playerB;
            if (_context.Players.Any(x => x.Name!.Equals(PlayerB) && x.PlayerRole == PlayerRole.B))
            {
                playerB = _context.Players.Single(x => x.Name!.Equals(PlayerB) && x.PlayerRole == PlayerRole.B);
            }
            else
            {
                var type = PlayerType.Human;
                if (GameType != "2H")
                {
                    type = PlayerType.AI;
                }
                playerB = new Player()
                {
                    Name = PlayerB!,
                    PlayerType = type,
                    PlayerRole = PlayerRole.B,
                };
            }

            var gameName = "Game_started_at:_" + DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss");
            if (GameName != null && !GameName.Equals(""))
            {
                gameName = GameName;
            }

            var game = new Game()
            {
                Description = DateTime.Now.ToString("yyyy-MM-dd"),
                GameName = gameName,
                GameOption = gameOptions,
                PlayerA = playerA,
                PlayerB = playerB,
                GameCreatedAt = DateTime.Now,
            };

            _context.Games.Add(game);
            await _context.SaveChangesAsync();
            
            return RedirectToPage("/GamePlay/Initiate", new { gameId = game.GameId } );
        }
    }
}
