﻿using System;

namespace MenuSystem
{
    public class MenuItem
    {
        public virtual string Label { get; set; }
        public virtual Func<string> MethodToExecute { get; set; }
            
        public MenuItem(string label, Func<string> methodToExecute)
        {
            Label = label.Trim();
            MethodToExecute = methodToExecute;
        }

        public override string ToString()
        {
            return $"{Label}".PadRight(70).PadLeft(72);
        }
    }
}