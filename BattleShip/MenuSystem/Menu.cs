﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MenuSystem
{
    public enum MenuLevel
    {
        Level0,
        Level1,
        Level2Plus
    }

    public class Menu
    {
        private List<MenuItem> MenuItems { get; set; } = new List<MenuItem>();
        private readonly MenuLevel _menuLevel;
        private static bool _haveRunWelcome;
        private static bool _clearConsole;
        private readonly string[] _illegalLabels = {"exit", "return to main", "return to previous"};
        private static string _gameName = "The game has no name.";
        private static readonly int CursorPosition = Console.WindowWidth / 2 - _gameName.Length;
        private static bool _doNotExecuteMethod;

        public Menu(MenuLevel level)
        {
            _menuLevel = level;
        }

        public static bool ClearConsole
        {
            get => _clearConsole;
            set => _clearConsole = value;
        }
        
        public static string GameName
        {
            get => _gameName;
            set => _gameName = value;
        }
        
        public static bool HaveRunWelcome
        {
            get => _haveRunWelcome;
            set => _haveRunWelcome = value;
        }

        public void AddMenuItem(string label, Func<string> methodToExecute)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.White;

            if (_illegalLabels.Contains(label.Trim()))
            {
                label = $"__{label.Trim()}";
            }

            if (label.Trim().Length > 50)
            {
                label = label.Trim().Substring(0, 50);
            }
            
            if (label.Trim().Length < 2)
            {
                string message =
                    $"\n \n :(    Set up failed. Label << {label.Trim()} >> is too short. \n";
                throw new Exception(message);
            }

            if (MenuItems.Any(p => p.Label == label.Trim()))
            {
                string message =
                    "\n \n :(    Set up failed. Sub-menu cannot have multiple same labels or same user choices.\n";
                throw new Exception(message);
            }

            Console.ResetColor();

            MenuItem newMenuItem = new MenuItem(label, methodToExecute);
            MenuItems.Add(newMenuItem);
        }

        public string RunMenu()
        {
            if (_menuLevel == MenuLevel.Level0 && !_haveRunWelcome)
            {
                RunWelcomeScreen();
            }

            string returnType = DisplayMenuItems();

            returnType = ActOnReturnType(returnType);
            
            return returnType;
        }

        public string ActOnReturnType(string returnType)
        {
            if (returnType == "toMain" && _menuLevel == MenuLevel.Level0)
            {
                returnType = RunMenu();
            }

            if (returnType == "oneDown")
            {
                return "0";
            }
            if (returnType == "0")
            {
                returnType = RunMenu();
            }
            
            if (returnType == "exit" && _menuLevel == MenuLevel.Level0)
            {
                returnType = ActionOnExit();
            }

            return returnType;
        }
        
        public void RunWelcomeScreen()
        {
            _haveRunWelcome = true;
            Console.Clear();
            Console.SetCursorPosition(CursorPosition, 10);
            Console.BackgroundColor = ConsoleColor.Cyan;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine($"==========>>> {_gameName} <<<==========");
            Console.SetCursorPosition(CursorPosition, 14);
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("Please press any key to enter the menu".PadRight(28 + _gameName.Length, '.'));
            Console.ResetColor();
            Console.SetCursorPosition(CursorPosition, 18);
            Console.ReadKey();
        }

        private string DisplayMenuItems()
        {
            AddBuiltInMenuItems();
            ConsoleKeyInfo key;
            int currentItem = 0;
            do
            {
                if (_clearConsole) Console.Clear();
                // ClearConsole = true;
                Console.SetCursorPosition(0, 1);
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine($"=====>{_gameName}<===What do you want to do?".PadRight(Console.WindowWidth, '='));
                Console.SetCursorPosition(CursorPosition, 10);
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine("Please navigate with up and down arrow buttons.".PadRight(70).PadLeft(72));
                Console.ResetColor();

                for (int i = 0; i < MenuItems.Count; i++)
                {
                    if (currentItem == i)
                    {
                        Console.SetCursorPosition(CursorPosition, 10 + i + 1);
                        Console.BackgroundColor = ConsoleColor.DarkMagenta;
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine(MenuItems[i]);
                    }
                    else
                    {
                        if (MenuItems[i].Label.ToLower().Equals("exit"))
                        {
                            Console.SetCursorPosition(CursorPosition, 10 + i + 1);
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.WriteLine(MenuItems[i]);
                        }
                        else
                        {
                            Console.SetCursorPosition(CursorPosition, 10 + i + 1);
                            Console.BackgroundColor = _menuLevel == MenuLevel.Level0
                                ? ConsoleColor.Cyan
                                : ConsoleColor.DarkCyan;
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine(MenuItems[i]);
                        }
                    }
                }

                Console.ResetColor();

                key = Console.ReadKey(true);

                if (key.Key.ToString() == "DownArrow")
                {
                    currentItem++;
                    if (currentItem > MenuItems.Count - 1) currentItem = 0;
                }
                else if (key.Key.ToString() == "UpArrow")
                {
                    currentItem--;
                    if (currentItem < 0) currentItem = Convert.ToInt16(MenuItems.Count - 1);
                }
            } while (key.KeyChar != 13);

            if (!_doNotExecuteMethod)
            {
                return MenuItems[currentItem].MethodToExecute();
            }
            _doNotExecuteMethod = false;
            return currentItem.ToString();


        }
        
        private void AddBuiltInMenuItems()
        {
            if (MenuItems.Any(p => p.Label.ToLower() == "exit"))
            {
                return;
            }
            switch (_menuLevel)
            {
                case MenuLevel.Level0:
                    MenuItems.Add(new MenuItem("Exit", ExitReturn));
                    break;
                case MenuLevel.Level1:
                    MenuItems.Add(new MenuItem("Return to Main" , ActionOnMain));
                    MenuItems.Add(new MenuItem("Exit", ExitReturn));
                    break;
                default:
                    MenuItems.Add(new MenuItem("Return to previous", ActionOnReturn));
                    MenuItems.Add(new MenuItem("Return to Main", ActionOnMain));
                    MenuItems.Add(new MenuItem("Exit", ExitReturn));
                    break;
            }
        }

        private string ExitReturn()
        {
            return "exit";
        }

        private static string ActionOnExit()
        {
            Console.Clear();
            Console.SetCursorPosition(CursorPosition, 15);
            Console.WriteLine("Closing down....");
            Console.WriteLine();
            System.Threading.Thread.Sleep(1500);
            
            return "exit";
        }

        private string ActionOnMain()
        {
            return "toMain";
        }

        private string ActionOnReturn()
        {
            return "oneDown";
        }
    }
    
}