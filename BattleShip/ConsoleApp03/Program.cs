﻿using System;
using System.IO;
using System.Linq;
using ComsoleApp02;
using GameBrain;
using GameConsoleUI;
using MenuSystem;

namespace ConsoleApp02
{
    internal class Program
    {
        private static Battleship _game = null!;

        public static Battleship GetGame
        {
            get => _game; 
            set => _game = value;
        }

        private static void Main()
        {
            _game = new Battleship(SetUpUI.BoardHeight, SetUpUI.BoardWidth, SetUpUI.ShipSizes, SetUpUI.CountOfShips,
                SetUpUI.MayHaveTurnsInRow, SetUpUI.ShipMargin);
            SetUpMenu();
        }

        private static string SetUpMenu()
        {
            Menu.ClearConsole = true;
            const string? gameName = "BATTLESHIP";
            Menu.GameName = gameName;

            var setMarginRuleMenu = new Menu(MenuLevel.Level2Plus);
            setMarginRuleMenu.AddMenuItem("Ships' sides and corners can touch", () => SetUpUI.SetMarginRule(1));
            setMarginRuleMenu.AddMenuItem("Only ships' sides cannot touch", () => SetUpUI.SetMarginRule(2));
            setMarginRuleMenu.AddMenuItem("Ships cannot touch each other", () => SetUpUI.SetMarginRule(3));

            var setTurnRuleMenu = new Menu(MenuLevel.Level2Plus);
            setTurnRuleMenu.AddMenuItem("Yes, Player can have a new try when they hit a ship!",
                () => SetUpUI.SetTurnRule(true));
            setTurnRuleMenu.AddMenuItem("No, Player cannot have a new try when they hit a ship!",
                () => SetUpUI.SetTurnRule(false));

            var menuB1 = new Menu(MenuLevel.Level1);
            menuB1.AddMenuItem("Human vs Computer", () => Battleship("1AI"));
            menuB1.AddMenuItem("Human vs Human", () => Battleship("humans"));
            menuB1.AddMenuItem("Computer vs Computer", () => Battleship("2AI"));

            var menuB = new Menu(MenuLevel.Level1);
            menuB.AddMenuItem("Select board size", SetUpUI.SetBoardSize);
            menuB.AddMenuItem("Select ship sizes", SetUpUI.SetShipSize);
            menuB.AddMenuItem("Select ship counts", SetUpUI.SetShipCount);
            menuB.AddMenuItem("Can player have multiple turns in a row?", setTurnRuleMenu.RunMenu);
            menuB.AddMenuItem("Can ship touch other's side/corner?", setMarginRuleMenu.RunMenu);
            menuB.AddMenuItem("See current parameters", SetUpUI.ViewParameters);
            menuB.AddMenuItem("Start the game", menuB1.RunMenu);

            
            var menuA1 = new Menu(MenuLevel.Level1);
            menuA1.AddMenuItem("Human vs Computer", () => Battleship("default1AI"));
            menuA1.AddMenuItem("Human vs Human", () => Battleship("defaultHumans"));
            menuA1.AddMenuItem("Computer vs Computer", () => Battleship("default2AI"));
            
            var menu = new Menu(MenuLevel.Level0);
            menu.AddMenuItem("Play with default settings", menuA1.RunMenu);
            menu.AddMenuItem("Play with custom settings", menuB.RunMenu);
            menu.AddMenuItem("Load a saved game from local file", RunGetSavedGameMenu);
            menu.AddMenuItem("Load a saved game from DB", GameDbSave.RunGetSavedGameMenu);


            menu.RunMenu();

            return "";
        }

        internal static string Battleship(string modifier)
        {
            if (modifier.Equals("defaultHumans") || modifier.Equals("default1AI") || modifier.Equals("default2AI"))
            {
                SetUpUI.ResetToDefaults();
            }
            if (modifier != "fromSave")
            {
                var keys = SetUpUI.ShipSizes.Keys;
                foreach (var key in keys)
                {
                    if (SetUpUI.ShipSizes[key] == 0)
                    {
                        SetUpUI.CountOfShips[key] = 0;
                    }
                }

                if (SetUpUI.CountOfShips.Values.Max() == 0)
                {
                    Console.Clear();
                    Console.WriteLine("You cannot play without any ships. Please fix your configuration!");
                    Console.ReadKey();
                    return "toMain";
                }
                var boardCheck = GameBrain.Battleship.CheckGameConfiguration(SetUpUI.BoardWidth, SetUpUI.BoardHeight,
                    SetUpUI.CountOfShips, SetUpUI.ShipSizes, SetUpUI.ShipMargin);
                if (boardCheck != 0)
                {
                    SetUpUI.BoardWidth = boardCheck;
                    SetUpUI.BoardHeight= boardCheck;
                    Console.Clear();
                    Console.WriteLine("Your board could not hold your ships. Board size has been updated to minimum viable size. Press Any key to continue.");
                    Console.ReadKey();
                }
                
                _game = new Battleship(SetUpUI.BoardHeight, SetUpUI.BoardWidth, SetUpUI.ShipSizes, SetUpUI.CountOfShips,
                    SetUpUI.MayHaveTurnsInRow, SetUpUI.ShipMargin);

                if (modifier.Equals("humans") || modifier.Equals("defaultHumans"))
                {
                    _game.GetPlayerA = new Player(
                        SetUpUI.GetPlayerNames(PlayerRole.A),
                        PlayerRole.A,
                        PlayerType.Human
                    );

                    _game.GetPlayerB = new Player(
                        SetUpUI.GetPlayerNames(PlayerRole.B),
                        PlayerRole.B,
                        PlayerType.Human
                    );
                }
                
                if (modifier.Equals("1AI") || modifier.Equals("default1AI"))
                {
                    _game.GetPlayerA = new Player(
                        SetUpUI.GetPlayerNames(PlayerRole.A),
                        PlayerRole.A,
                        PlayerType.Human
                    );

                    _game.GetPlayerB = new Player(
                        "Nemesis",
                        PlayerRole.B,
                        PlayerType.AI
                    );
                }
                
                if (modifier.Equals("2AI") || modifier.Equals("default2AI"))
                {
                    _game.GetPlayerA = new Player(
                        "Genesis",
                        PlayerRole.A,
                        PlayerType.AI
                    );

                    _game.GetPlayerB = new Player(
                        "Nemesis",
                        PlayerRole.B,
                        PlayerType.AI
                    );
                }

                ConsoleUi.DrawBoard(_game.GetBoardForA(), _game.GetBoardForB(), _game.NextMoveByPlayerA, _game);
                Console.Clear();
                ConsoleUi.SetBoardsUp(_game);
            }

            var shipsOnBoard = _game.GetShipsPerPlayerOnBoard;

            do
            {
                Console.Clear();
                ConsoleUi.DrawBoard(_game.GetBoardForA(), _game.GetBoardForB(), _game.NextMoveByPlayerA, _game);

                var playerInFocus = _game.NextMoveByPlayerA ? _game.GetPlayerA : _game.GetPlayerB;

                if (playerInFocus!.Type == PlayerType.Human)
                {
                    var inGameMenu = new Menu(MenuLevel.Level0);
                
                    inGameMenu.AddMenuItem(
                        $"Player {(_game.NextMoveByPlayerA ? _game.GetPlayerA!.Name : _game.GetPlayerB!.Name)}, please make a move.",
                        () => GetMoveCoordinates(_game));
                    inGameMenu.AddMenuItem("See your board", () =>
                    {
                        Console.Clear();
                        ConsoleUi.DrawBoard(_game.GetBoardForA(), _game.GetBoardForB(), _game.NextMoveByPlayerA, _game);
                        Console.ResetColor();
                        Console.ReadKey();
                        return "";
                    });
                    inGameMenu.AddMenuItem("Return to initial setup", SetUpMenu);
                    inGameMenu.AddMenuItem("Save game", () => SaveGameAction(_game));
                    inGameMenu.AddMenuItem("Save game to DB", () => GameDbSave.SaveGame(_game));

                    Menu.ClearConsole = false;
                    Menu.HaveRunWelcome = true;

                    var userChoice = inGameMenu.RunMenu();
                    Console.WriteLine(userChoice);
                    if (userChoice == "exit") return "exit";
                }
                else
                {
                    Console.ResetColor();
                    GetMoveCoordinates(_game);
                }
                
    
                if (_game.GetShipsSankByA == shipsOnBoard || _game.GetShipsSankByB == shipsOnBoard)
                {
                    ConsoleUi.WinnerScreen(_game);
                    return "toMain";
                }
                
            } while (_game.GetShipsSankByA != shipsOnBoard || _game.GetShipsSankByB != shipsOnBoard);

            ConsoleUi.WinnerScreen(_game);

            return "toMain";
        }

        private static string GetMoveCoordinates(Battleship game)
        {
            Console.ResetColor();
            var res = Bombing.MakeAMove(game);

            return res;
        }

        private static string SaveGameAction(Battleship game)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            var defaultName = "SavedGame_" + DateTime.Now.ToString("yyyy-MM-dd") + ".json";
            Console.Write($"File name ({defaultName}): ");
            var fileName = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(fileName))
                fileName = defaultName;
            else
                fileName = "SavedGame_" + fileName + ".json";

            var serializedGame = game.GetSerializedGameState();
            File.WriteAllText(fileName, serializedGame);
            Console.ResetColor();
            return "";
        }

        private static string RunGetSavedGameMenu()
        {
            var files = (from file in Directory.EnumerateFiles(".", "*.json")
                where file.Contains("SavedGame")
                select file).ToList();

            var savedGameSelector = new Menu(MenuLevel.Level1);
            for (var i = 0; i < files.Count; i++)
            {
                var i1 = i;
                savedGameSelector.AddMenuItem($"({i}) {files[i]}",
                    () => LoadGameAction(i1));
            }

            Console.WriteLine(files.Count);
            return savedGameSelector.RunMenu();
        }

        private static string LoadGameAction(int fileNo)
        {
            var files = (from file in Directory.EnumerateFiles(".", "*.json")
                where file.Contains("SavedGame")
                select file).ToList();

            var fileName = files[fileNo];
            Console.WriteLine(fileNo);
            var jsonString = File.ReadAllText(fileName);

            _game.SetGameStateFromJsonString(jsonString);

            ConsoleUi.ReloadedFromSaveScreen(_game);
            Console.Clear();
            Battleship("fromSave");
            return "";
        }
    }
}