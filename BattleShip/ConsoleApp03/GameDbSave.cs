﻿using System;
using System.Linq;
using System.Text.Json;
using ConsoleApp02;
using DAL;
using Domain;
using GameBrain;
using GameConsoleUI;
using MenuSystem;
using Microsoft.EntityFrameworkCore;
using Player = Domain.Player;

namespace ComsoleApp02
{
    public class GameDbSave
    {
        public static string SaveGame(Battleship gameToSave)
        {

            using var db = new AppDbContext(DbContextOptionsBuilder().Options);

            db.Database.Migrate();

            Player playerA;
            if (db.Players.Any(x => x.Name!.Equals(gameToSave.GetPlayerA!.Name) && x.PlayerRole == PlayerRole.A))
            {
                playerA = db.Players.Single(x => x.Name!.Equals(gameToSave.GetPlayerA!.Name) && x.PlayerRole == PlayerRole.A);
            }
            else
            {
                playerA = new Player
                {
                    Name = gameToSave.GetPlayerA!.Name,
                    PlayerType = gameToSave.GetPlayerA!.Type,
                    PlayerRole = gameToSave.GetPlayerA!.Role
                };
            }
            Player playerB;
            if (db.Players.Any(x => x.Name!.Equals(gameToSave.GetPlayerB!.Name) && x.PlayerRole == PlayerRole.B))
            {
                playerB = db.Players.Single(x => x.Name!.Equals(gameToSave.GetPlayerB!.Name) && x.PlayerRole == PlayerRole.B);
            }
            else
            {
                playerB = new Player
                {
                    Name = gameToSave.GetPlayerB!.Name,
                    PlayerType = gameToSave.GetPlayerB!.Type,
                    PlayerRole = gameToSave.GetPlayerB!.Role
                };
            }
            Game game;
            if (gameToSave.GetGameId == null)
                game = new Game
                {
                    GameName = DbUi.AskForSavingName("game"),
                    GameOption = new GameOption
                    {
                        BoardWidth = gameToSave.GetBoardWidth,
                        BoardHeight = gameToSave.GetBoardHeight,
                        MarginRule = gameToSave.GetShipMargin,
                        TurnRule = gameToSave.GetTurnRule,
                        ShipSizes = Battleship.GetSerializedDictionary(gameToSave.GetShipSizes),
                        ShipCounts = Battleship.GetSerializedDictionary(gameToSave.GetShipCounts)
                    },
                    
                    PlayerA = playerA,
                    PlayerB = playerB,
                    GameCreatedAt = DateTime.Now
                };
            else
                game = db.Games.Single(g => g.GameId == gameToSave.GetGameId);


            var board = new Board
            {
                BoardName = DbUi.AskForSavingName("board"),
                
                Winner = gameToSave.GetWinner,
                NextTurnByWho = gameToSave.NextMoveByPlayerA,

                PlayerABoard = Battleship.GetSerializedBoard(gameToSave.GetBoardPlayerA!),
                PlayerBBoard = Battleship.GetSerializedBoard(gameToSave.GetBoardPlayerB!),

                PlayerAShips = Battleship.GetSerializedDictionary(gameToSave.GetPlayerAShips),
                PlayerBShips = Battleship.GetSerializedDictionary(gameToSave.GetPlayerBShips),

                ShipsSankByA = gameToSave.GetShipsSankByA,
                ShipsSankByB = gameToSave.GetShipsSankByB,

                Game = game,
                
                CreatedAt = DateTime.Now
            };

            db.Boards.Add(board);
            db.SaveChanges();
            gameToSave.GetGameId = game.GameId;

            return "";
        }

        private static DbContextOptionsBuilder<AppDbContext> DbContextOptionsBuilder()
        {
            var dbOptions = new DbContextOptionsBuilder<AppDbContext>();
            dbOptions
                .EnableSensitiveDataLogging()
                .UseSqlServer(@"
                    Server=barrel.itcollege.ee,1533; 
                    User Id=student;
                    Password=Student.Bad.password.0;
                    Database=mihkel.heinmaa_BattleshipTestingDb;
                    MultipleActiveResultSets=true"
                );
            return dbOptions;
        }

        public static string LoadGameFromDb(int boardId)
        {
            var dbOptions = DbContextOptionsBuilder();
            using var db = new AppDbContext(dbOptions.Options);
            
            db.Database.Migrate();

            var state = db.Boards
                .Include(x => x.Game).ThenInclude(g => g!.GameOption)
                .Include(x => x.Game).ThenInclude(g => g!.PlayerA)
                .Include(x => x.Game).ThenInclude(g => g!.PlayerB)
                .Single(b => b.BoardId == boardId);

            Program.GetGame.SetGameStateFromDb(state);

            ConsoleUi.ReloadedFromSaveScreen(Program.GetGame);
            Console.Clear();
            Program.Battleship("fromSave");
            return "";
        }


        public static string RunGetSavedGameMenu()
        {
            
            var dbOptions = DbContextOptionsBuilder();
            using var db = new AppDbContext(dbOptions.Options);

            db.Database.Migrate();
            
            var savedGameSelector = new Menu(MenuLevel.Level1);

            foreach (var game in db.Games
                .Include(p => p.PlayerA)
                .Include(p => p.PlayerB))
            {
                savedGameSelector.AddMenuItem(game.ToString(), () => RunGetSavedBoardMenu(game.GameId));
            }

            return savedGameSelector.RunMenu();
        }

        public static string RunGetSavedBoardMenu(int gameId)
        {
            var dbOptions = DbContextOptionsBuilder();
            using var db = new AppDbContext(dbOptions.Options);

            db.Database.Migrate();
            var savedBoardSelector = new Menu(MenuLevel.Level2Plus);

            foreach (var board in db.Boards.Where(b => b.GameId == gameId))
            {
                savedBoardSelector.AddMenuItem(board.ToString(), () => LoadGameFromDb(board.BoardId));
            }

            return savedBoardSelector.RunMenu();
        }
    }
    
}