﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Enums;
using GameBrain;

namespace GameConsoleUI
{
    public class GameInitialization
    {
        private static readonly CellState[,] Board = ConsoleUi.BoardLeft;
        private static readonly int Width = ConsoleUi.Width;

        public static void PlaceTheShips(CellState[,] board, Player player, Battleship game)
        {
            const int startX = 30;
            const int startY = 6;
            var optionsPerLine = SetUpUI.BoardWidth;
            const int spacingPerLine = 3;

            var nextShipsDirection = "horizontal";

            var currentSelection = 0;
            int currentRow;
            int currentColumn;
            int colIndex;
            int rowIndex;

            ConsoleKey key;

            Console.CursorVisible = false;
            var shipSizes = game.GetShipSizes!.Values.ToArray();
            var shipCounts = game.GetShipCounts!.Values.ToArray();
            var shipNames = game.GetShipCounts!.Keys.ToArray();

            var shipCount = 1;
            var pointer = 0;

            while (pointer < 5)
            {
                var shipCountLeft = shipCounts[pointer];
                while (shipCountLeft > 0)
                {
                    do
                    {
                        Console.Clear();
                        ConsoleUi.PrintHeader(
                            $"Please mark your type   {shipNames[pointer]}   ships into the board. These are size   {shipSizes[pointer]}   ships and you have   {shipCountLeft}   left to put.");
                        Console.WriteLine(
                            $"Your next ship will be placed   -->{nextShipsDirection}ly<--.   To change placing direction, please press   V   for vertical or   H   for horizontal.");
                        Console.WriteLine(
                            "Ship is placed from the selected position to the   right   or   downwards   accordingly.");

                        for (var i = 0; i < board.Length; i++)
                        {
                            Console.BackgroundColor = ConsoleColor.Blue;
                            Console.SetCursorPosition(startX + i % optionsPerLine * spacingPerLine,
                                startY + i / optionsPerLine);

                            if (i == currentSelection) Console.ForegroundColor = ConsoleColor.Red;

                            colIndex = i % Width;
                            rowIndex = i / Width;
                            Console.Write(ConsoleUi.CellString(board[colIndex, rowIndex]));
                            Console.ResetColor();
                        }

                        key = Console.ReadKey(true).Key;

                        switch (key)
                        {
                            case ConsoleKey.H:
                                nextShipsDirection = "horizontal";
                                break;
                            case ConsoleKey.V:
                                nextShipsDirection = "vertical";
                                break;
                            case ConsoleKey.LeftArrow:
                            {
                                if (currentSelection % optionsPerLine > 0)
                                    currentSelection--;
                                break;
                            }
                            case ConsoleKey.RightArrow:
                            {
                                if (currentSelection % optionsPerLine < optionsPerLine - 1)
                                    currentSelection++;
                                break;
                            }
                            case ConsoleKey.UpArrow:
                            {
                                if (currentSelection >= optionsPerLine)
                                    currentSelection -= optionsPerLine;
                                break;
                            }
                            case ConsoleKey.DownArrow:
                            {
                                if (currentSelection + optionsPerLine < Board.Length)
                                    currentSelection += optionsPerLine;
                                break;
                            }
                        }
                    } while (key != ConsoleKey.Enter);

                    Console.CursorVisible = true;


                    currentColumn = currentSelection % Width;
                    currentRow = currentSelection / Width;

                    int[] result = new int[2];
                    result[0] = currentColumn;
                    result[1] = currentRow;

                    var markingResult = game.MarkShipToGameBoard(result, nextShipsDirection, player, shipSizes[pointer], shipCount);
                    
                    if (markingResult.Equals(""))
                    {
                        MarkCheckedPositionsLocally(board, nextShipsDirection, result, shipSizes[pointer]);
                        shipCount++;
                        shipCountLeft--;
                    }
                    else
                    {
                        ThrowWarnings(markingResult);
                    }
                }

                pointer++;
            }
        }

        private static void MarkCheckedPositionsLocally(CellState[,] board, string direction, int[] xy, int size)
        {
            var x = xy[0];
            var y = xy[1];

            while (size > 0)
            {
                board[x, y] = CellState.Ship;
                if (direction == "horizontal")
                    x++;
                else
                    y++;

                size--;
            }

        }

        public static void ThrowWarnings(string message)
        {
            Console.Clear();
            Console.ResetColor();
            Console.SetCursorPosition(10, 5);

            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine(message);
            Console.ResetColor();
            Console.SetCursorPosition(10, 6);
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }
    }
}