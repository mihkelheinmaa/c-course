﻿using System;
using Domain.Enums;
using GameBrain;

namespace GameConsoleUI
{
    public class Bombing
    {
        private static CellState[,] _board = ConsoleUi.BoardRight;
        private static int _width;
        private static int _height;

        public static string MakeAMove(Battleship game)
        {
            _width = game.GetBoardWidth;
            _height = game.GetBoardHeight;
            _board = game.GetBoardForB();

            int[] xy = {-1, -1};
            if (game.NextMoveByPlayerA && game.GetPlayerA!.Type != PlayerType.AI)
            {
                var tmp = new CellState[_width, _height];
                Array.Copy(_board, tmp, _board.Length);
                for (var rowIndex = 0; rowIndex < _height; rowIndex++)
                for (var colIndex = 0; colIndex < _width; colIndex++)
                    if (tmp[colIndex, rowIndex] == CellState.Ship)
                        tmp[colIndex, rowIndex] = CellState.Free;

                xy = ConsoleUi.Navigation(tmp, game.GetBoardWidth);
            }
            else if ( game.GetPlayerB!.Type != PlayerType.AI)
            {
                var tmp = new CellState[_width, _height];
                Array.Copy(game.GetBoardForA(), tmp, _board.Length);
                for (var rowIndex = 0; rowIndex < _height; rowIndex++)
                for (var colIndex = 0; colIndex < _width; colIndex++)
                    if (tmp[colIndex, rowIndex] == CellState.Ship)
                        tmp[colIndex, rowIndex] = CellState.Free;

                xy = ConsoleUi.Navigation(tmp, game.GetBoardWidth);
            }

            DropTheBomb(xy, game);
            return "";
        }


        public static void DropTheBomb(int[] xy, Battleship game)
        {
            Player player = (game.NextMoveByPlayerA ? game.GetPlayerA : game.GetPlayerB)!;
           
            if (player.Type == PlayerType.Human)
            {
                var moveResult = game.MakeAMove(xy, player);

                if (moveResult == MoveReturn.Miss)
                {
                    Console.Clear();
                    ConsoleUi.PrintHeader("PRESS ANY KEY TO CONTINUE");
                    Console.SetCursorPosition(15, 5);
                    Console.BackgroundColor = ConsoleColor.DarkBlue;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("It was a   MISS   ! Your turn is over.");
                    Console.ResetColor();
                    Console.ReadKey();
                }

                else if (moveResult != MoveReturn.SameSpotTwice)
                {
                    Console.Clear();
                    ConsoleUi.PrintHeader("PRESS ANY KEY TO CONTINUE");
                    Console.ResetColor();

                    Console.SetCursorPosition(15, 5);
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.White;
                    if (moveResult == MoveReturn.HitAndSankRetry)
                    {
                        Console.WriteLine("!!!  YOU'VE SANK AN ENEMY'S SHIP !!! You can drop another bomb.");
                    }
                    else if (moveResult == MoveReturn.HitAndSankNoRetry)
                    {
                        Console.WriteLine("!!!  YOU'VE SANK AN ENEMY'S SHIP !!! Your turn is over.");
                    }
                    else if (moveResult == MoveReturn.HitRetry)
                    {
                        Console.WriteLine("It was a   HIT   ! You can drop another bomb.");
                    }
                    else if (moveResult == MoveReturn.HitNoRetry)
                    {
                        Console.WriteLine("It was a   HIT   ! Your turn is over.");
                    }

                    Console.ResetColor();
                    Console.ReadKey();
                }

                else
                {
                    Console.Clear();
                    ConsoleUi.PrintHeader("PRESS ANY KEY TO CONTINUE");
                    Console.ResetColor();
                    Console.SetCursorPosition(15, 5);
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Don't be stupid - don't bomb the same spot twice! Please try again.");
                    Console.ResetColor();
                    Console.ReadKey();
                }
            }
            else
            {
                AI.MakeAMove(game, player);
            }
        }
    }
}