﻿using System;
using System.Linq;
using Domain.Enums;
using GameBrain;
using MenuSystem;

namespace GameConsoleUI
{
    public static class ConsoleUi
    {
        private static CellState[,] _boardLeft = null!;
        private static CellState[,] _boardRight = null!;
        private static int _width;
        private static int _height;

        public static CellState[,] BoardLeft
        {
            get => _boardLeft;
            set => _boardLeft = value;
        }

        public static CellState[,] BoardRight
        {
            get => _boardRight;
            set => _boardRight = value;
        }

        public static int Width
        {
            get => _width;
            set => _width = value;
        }

        public static int Height
        {
            get => _height;
            set => _height = value;
        }

        public static void DrawBoardNow(int leftOffset, CellState[,] board, string title, string stats)
        {
            PrintHeader("What do want to do?");

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            var i = 3;
            Console.SetCursorPosition(leftOffset, i);
            Console.WriteLine(stats);
            i += 3;
            Console.SetCursorPosition(leftOffset, i);
            Console.WriteLine(title);
            i += 2;
            for (var rowIndex = 0; rowIndex < _height; rowIndex++)
            {
                Console.SetCursorPosition(leftOffset, i);
                i++;
                for (var colIndex = 0; colIndex < _width; colIndex++)
                    Console.Write(CellString(board[colIndex, rowIndex]));

                Console.WriteLine();
            }
        }

        public static void DrawBoard(CellState[,] board1, CellState[,] board2, bool turn, Battleship game)
        {
            var statsA = game.GetStats(game.GetPlayerA!).OrderBy(s=> s.GetSize);
            var statsStringA = $"Player {game.GetPlayerA!.Name} has on board left: ";
            foreach (var s in statsA)
            {
                statsStringA += $"{s.GetCountLeft}/{s.GetInitialCount} size {s.GetSize} ships | ";
            }
            var statsB = game.GetStats(game.GetPlayerB!).OrderBy(s=> s.GetSize);
            var statsStringB = $"Player {game.GetPlayerB!.Name} has on board left: ";
            foreach (var s in statsB)
            {
                statsStringB += $"{s.GetCountLeft}/{s.GetInitialCount} size {s.GetSize} ships | ";
            }
 
            var stats = $"{statsStringA} \n{statsStringB} ";
            
            string titleSelf = "Bombings on You";
            string titleEnemy = "Your bombings";
            _boardLeft = board1;
            _boardRight = board2;
            _width = _boardLeft.GetUpperBound(0) + 1;
            _height = _boardLeft.GetUpperBound(1) + 1;

            if (turn)
            {
                _boardLeft = board1;
                var tmp = new CellState[_width, _height];
                Array.Copy(board2, tmp, _boardRight.Length);
                for (var rowIndex = 0; rowIndex < _height; rowIndex++)
                for (var colIndex = 0; colIndex < _width; colIndex++)
                    if (tmp[colIndex, rowIndex] == CellState.Ship)
                        tmp[colIndex, rowIndex] = CellState.Free;

                DrawBoardNow(0, _boardLeft, titleSelf, stats);
                DrawBoardNow(_width * 3 + 7, tmp, titleEnemy, "");
            }
            else
            {
                _boardLeft = board2;
                var tmp = new CellState[_width, _height];
                Array.Copy(board1, tmp, _boardRight.Length);
                for (var rowIndex = 0; rowIndex < _height; rowIndex++)
                for (var colIndex = 0; colIndex < _width; colIndex++)
                    if (tmp[colIndex, rowIndex] == CellState.Ship)
                        tmp[colIndex, rowIndex] = CellState.Free;

                DrawBoardNow(0, _boardLeft, titleSelf, stats);
                DrawBoardNow(_width * 3 + 7, tmp, titleEnemy, "");
            }
        }

        public static void PrintHeader(string message)
        {
            Console.SetCursorPosition(0, 1);
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine($"=====>{Menu.GameName}<==={message}".PadRight(Console.WindowWidth, '='));
            Console.ResetColor();
        }

        public static int PrompterText(string question, int min, int max)
        {
            Console.Clear();

            PrintHeader("Please make a choice.");
            Console.ResetColor();
            var choiceInNumber = 0;

            do
            {
                Console.ResetColor();
                Console.SetCursorPosition(10, 5);

                Console.BackgroundColor = ConsoleColor.DarkBlue;
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine(question.PadLeft(40).PadRight(60));
                Console.ResetColor();
                Console.SetCursorPosition(10, 6);
                Console.WriteLine($"Please write a NUMBER! This must be between {min} and {max}");
                Console.SetCursorPosition(10, 8);
                Console.BackgroundColor = ConsoleColor.DarkBlue;
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(">");
                Console.ResetColor();
                var userChoice = Console.ReadLine()?.Trim() ?? "";

                try
                {
                    choiceInNumber = int.Parse(userChoice);
                }
                catch (FormatException)
                {
                    Console.SetCursorPosition(10, 6);
                }
            } while (choiceInNumber < min || choiceInNumber > max);

            return choiceInNumber;
        }

        public static string CellString(CellState cellState)
        {
            switch (cellState)
            {
                case CellState.Free: return "[ ]";
                case CellState.Hit: return "[x]";
                case CellState.Miss: return "[o]";
                case CellState.Ship: return "[@]";
            }

            return "-";
        }

        public static string SetBoardsUp(Battleship game)
        {
            if (game.GetPlayerA!.Type == PlayerType.Human)
            {
                Console.Clear();
                PrintHeader($"Player {game.GetPlayerA!.Name}, Please deport your fleet! --- PRESS ANY KEY TO CONTINUE");
                Console.ResetColor();
                Console.ReadKey();
                GameInitialization.PlaceTheShips(game.GetBoardForA(), game.GetPlayerA!, game);
                Console.Clear();
                Console.ResetColor();
            }
            else
            {
                AI.PlaceShips(game, "A");
            }

            if (game.GetPlayerB!.Type == PlayerType.Human)
            {
                PrintHeader($"Player {game.GetPlayerB!.Name}, Please deport your fleet! --- PRESS ANY KEY TO CONTINUE");
                Console.ResetColor();
                Console.ReadKey();
                GameInitialization.PlaceTheShips(game.GetBoardForB(), game.GetPlayerB!, game);
                Console.Clear();
                Console.ResetColor();
            }
            else
            {
                AI.PlaceShips(game, "B");
            }
            

            return "";
        }

        public static int[] Navigation(CellState[,] board, int width)
        {
            const int startX = 15;
            const int startY = 8;
            var optionsPerLine = width;
            const int spacingPerLine = 3;

            var currentSelection = 0;
            int currentRow;
            int currentColumn;
            int colIndex;
            int rowIndex;

            ConsoleKey key;

            Console.CursorVisible = false;

            do
            {
                Console.Clear();
                PrintHeader("Drop the b o m b !");

                for (var i = 0; i < board.Length; i++)
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.SetCursorPosition(startX + i % optionsPerLine * spacingPerLine,
                        startY + i / optionsPerLine);

                    if (i == currentSelection) Console.ForegroundColor = ConsoleColor.Red;

                    colIndex = i % _width;
                    rowIndex = i / _width;
                    Console.Write(CellString(board[colIndex, rowIndex]));
                    Console.ResetColor();
                }

                key = Console.ReadKey(true).Key;

                switch (key)
                {
                    case ConsoleKey.LeftArrow:
                    {
                        if (currentSelection % optionsPerLine > 0)
                            currentSelection--;
                        break;
                    }
                    case ConsoleKey.RightArrow:
                    {
                        if (currentSelection % optionsPerLine < optionsPerLine - 1)
                            currentSelection++;
                        break;
                    }
                    case ConsoleKey.UpArrow:
                    {
                        if (currentSelection >= optionsPerLine)
                            currentSelection -= optionsPerLine;
                        break;
                    }
                    case ConsoleKey.DownArrow:
                    {
                        if (currentSelection + optionsPerLine < _boardLeft.Length)
                            currentSelection += optionsPerLine;
                        break;
                    }
                }
            } while (key != ConsoleKey.Enter);

            Console.CursorVisible = true;


            currentColumn = currentSelection % _width;
            currentRow = currentSelection / _width;

            int[] result = new int[2];
            result[0] = currentColumn;
            result[1] = currentRow;

            Console.WriteLine(currentColumn);
            Console.WriteLine(currentRow);

            return result;
        }

        public static void WinnerScreen(Battleship game)
        {
            string winner = game.GetShipsSankByA > game.GetShipsSankByB ? game.GetPlayerA!.Name : game.GetPlayerB!.Name;

            PrintHeader("We have a winner!");

            Console.ResetColor();
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Black;

            Console.SetCursorPosition(20, 8);
            Console.WriteLine($"PLAYER {winner}, CONGRATULATIONS, YOU WON!");
            Console.ResetColor();

            Console.ReadKey();
        }

        public static void ReloadedFromSaveScreen(Battleship game)
        {
            Console.Clear();
            var nextPlayer = game.GetPlayerA!.Name;
            if (!game.NextMoveByPlayerA) nextPlayer = game.GetPlayerB!.Name;

            PrintHeader("Game loaded");
            Console.ResetColor();
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Black;

            Console.SetCursorPosition(20, 8);
            Console.WriteLine($"Player {nextPlayer}'s turn is next.");
            Console.ResetColor();
            Console.ReadKey();
        }
    }
}