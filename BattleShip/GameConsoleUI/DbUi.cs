﻿using System;

namespace GameConsoleUI
{
    public class DbUi
    {
        public static string AskForSavingName(string modifier)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            var defaultName = "SavedGame_" + DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss");
            if (modifier.Equals("game"))
                defaultName = "SavedGame_" + DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss");
            else if (modifier.Equals("board"))
                defaultName = "SavedBoard_" + DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss");
            Console.Write($"{modifier} name ({defaultName}): ");
            var fileName = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(fileName))
            {
                fileName = defaultName;
            }
            else
            {
                if (fileName.Trim().Length > 40)
                    fileName = fileName.Trim().Substring(0, 40);
                else
                    fileName = fileName.Trim();
            }

            Console.ResetColor();

            return fileName;
        }
    }
}