﻿using System;
using System.Collections.Generic;
using System.Threading;
using Domain.Enums;
using GameBrain;
using MenuSystem;

namespace GameConsoleUI
{
    public class SetUpUI
    {
        public static Dictionary<string, int> CountOfShips = new Dictionary<string, int>
            {{"Carrier", 1}, {"Battleship", 2}, {"Cruiser", 3}, {"Submarine", 4}, {"Destroyer", 5}};

        public static Dictionary<string, int> ShipSizes = new Dictionary<string, int>
            {{"Carrier", 5}, {"Battleship", 4}, {"Cruiser", 3}, {"Submarine", 2}, {"Destroyer", 1}};

        public static int BoardHeight = 15;
        public static int BoardWidth = 15;

        public static TurnRule MayHaveTurnsInRow = TurnRule.CanHaveTurnsInRow;
        public static MarginRule ShipMargin = MarginRule.CannotTouch;

        public static string[] ShipNames = {"Carrier", "Battleship", "Cruiser", "Submarine", "Destroyer"};


        public static void ResetToDefaults()
        {
            CountOfShips = new Dictionary<string, int>
                {{"Carrier", 1}, {"Battleship", 2}, {"Cruiser", 3}, {"Submarine", 4}, {"Destroyer", 5}};
            ShipSizes = new Dictionary<string, int>
                {{"Carrier", 5}, {"Battleship", 4}, {"Cruiser", 3}, {"Submarine", 2}, {"Destroyer", 1}};
            BoardHeight = 15;
            BoardWidth = 15;
            MayHaveTurnsInRow = TurnRule.CanHaveTurnsInRow;
            ShipMargin = MarginRule.CannotTouch;
        }

        public static void ThrowErrorMessage(string message, string fieldName, int value)
        {
            Console.Clear();
            ConsoleUi.PrintHeader("Something went wrong. Press any key to continue");
            Console.ResetColor();

            Console.SetCursorPosition(10, 5);
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine(message.PadRight(70));
            Console.SetCursorPosition(10, 6);
            Console.WriteLine($"The value of your {fieldName} was set to: {value}".PadRight(70));
            Console.ResetColor();
            Console.ReadKey();
        }

        public static string SetShipSize()
        {
            {
                Console.Clear();
                ConsoleUi.PrintHeader("How big will your ships be?");

                ShipSizes = new Dictionary<string, int>();
                var shipTypeIndex = 0;
                while (shipTypeIndex < 5)
                {
                    var question = $"What size is the type {ShipNames[shipTypeIndex]} ship?";
                    var choice = ConsoleUi.PrompterText(question, 0, 10);
                    if (choice > BoardWidth && choice > BoardHeight)
                    {
                        ThrowErrorMessage(
                            $"The size of your {ShipNames[shipTypeIndex]} is too big to fit into the board",
                            "ship size",
                            BoardWidth);
                        choice = BoardWidth;
                    }

                    ShipSizes.Add(ShipNames[shipTypeIndex], choice);
                    shipTypeIndex++;
                }

                Console.Clear();
                ConsoleUi.PrintHeader("How big will your ships be?");

                Console.WriteLine("Current sizes of your ships:");
                Console.WriteLine();
                foreach (var ship in ShipSizes)
                    Console.WriteLine($"Size of type {ship.Key}".PadRight(30) + $"{ship.Value}");

                Thread.Sleep(4000);

                return AddMenu(SetShipSize);
            }
        }

        public static string SetShipCount()
        {
            Console.Clear();
            ConsoleUi.PrintHeader("How many will you have?");

            CountOfShips = new Dictionary<string, int>();
            var shipTypeIndex = 0;
            while (shipTypeIndex < 5)
            {
                var question = $"How many ships of type {ShipNames[shipTypeIndex]} do you want?";
                var choice = ConsoleUi.PrompterText(question, 0, 10);
                CountOfShips.Add(ShipNames[shipTypeIndex], choice);
                shipTypeIndex++;
            }

            Console.Clear();
            ConsoleUi.PrintHeader("How many will you have?");

            Console.WriteLine("Current count of your ships:");
            Console.WriteLine();
            foreach (var ship in CountOfShips)
                Console.WriteLine($"Count of type {ship.Key}".PadRight(30) + $"{ship.Value}");
            Thread.Sleep(4000);
            return AddMenu(SetShipCount);
        }

        public static string SetBoardSize()
        {
            Console.Clear();
            ConsoleUi.PrintHeader("How vast is your sea?");

            var question1 = "What will be the HEIGHT of the board?";
            var height = ConsoleUi.PrompterText(question1, 10, 40);

            BoardHeight = height;
            var question2 = "What will be the WIDTH of the board?";
            var width = ConsoleUi.PrompterText(question2, 10, 40);

            BoardWidth = width;

            Console.Clear();
            ConsoleUi.PrintHeader("How vast is your sea?");

            Console.WriteLine("Current size of your board:");
            Console.WriteLine();
            Console.WriteLine("Height:".PadRight(30) + $"{BoardHeight}");
            Console.WriteLine("Width:".PadRight(30) + $"{BoardWidth}");

            Thread.Sleep(4000);
            return AddMenu(SetBoardSize);
        }

        public static string ViewParameters()
        {
            Console.Clear();
            ConsoleUi.PrintHeader("An overview of your navy.");

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("Current size of your board:".PadRight(30));
            Console.WriteLine();
            Console.WriteLine("Height:".PadRight(30) + $"{BoardHeight}");
            Console.WriteLine("Width:".PadRight(30) + $"{BoardWidth}");
            Console.WriteLine();
            Console.WriteLine("Current count of your ships:".PadRight(30));
            Console.WriteLine();
            foreach (var ship in CountOfShips)
                Console.WriteLine($"Count of type  {ship.Key}:".PadRight(30) + $"{ship.Value}");
            Console.WriteLine();
            Console.WriteLine("Current sizes of your ships:".PadRight(30));
            Console.WriteLine();
            foreach (var ship in ShipSizes)
                Console.WriteLine($"Size of type  {ship.Key}:".PadRight(30) + $"{ship.Value}");
            Console.WriteLine();
            switch (MayHaveTurnsInRow)
            {
                case TurnRule.CannotHaveTurnsInRow:
                    Console.WriteLine("Player CANNOT have multiple turns in a row.".PadRight(30));
                    break;
                default:
                    Console.WriteLine("Player CAN have multiple turns in a row.".PadRight(30));
                    break;
            }

            Console.WriteLine();
            switch (ShipMargin)
            {
                case MarginRule.CanTouchSides:
                    Console.WriteLine("SIDES and CORNERS of player's ships MAY touch.".PadRight(30));
                    break;
                case MarginRule.CanTouchCorners:
                    Console.WriteLine("ONLY CORNERS of player's ships MAY touch.".PadRight(30));
                    break;
                default:
                    Console.WriteLine("Player's ships corners and sides CANNOT touch.".PadRight(30));
                    break;
            }


            Console.WriteLine();
            Console.WriteLine("PRESS ANY KEY TO CONTINUE".PadRight(30));
            Console.ResetColor();
            Console.ReadKey();
            return "0";
        }

        public static string SetTurnRule(bool b)
        {
            if (!b)
                MayHaveTurnsInRow = TurnRule.CannotHaveTurnsInRow;
            else
                MayHaveTurnsInRow = TurnRule.CanHaveTurnsInRow;
            return "oneDown";
        }

        public static string SetMarginRule(int i)
        {
            switch (i)
            {
                case 1:
                    ShipMargin = MarginRule.CanTouchSides;
                    break;
                case 2:
                    ShipMargin = MarginRule.CanTouchCorners;
                    break;
                default:
                    ShipMargin = MarginRule.CannotTouch;
                    break;
            }

            return "oneDown";
        }

        public static string AddMenu(Func<string> methodToExecute)
        {
            var inGameMenu = new Menu(MenuLevel.Level0);
            inGameMenu.AddMenuItem("All good, what else can I do?",
                () => "oneDown");
            inGameMenu.AddMenuItem("Not good, let me do it again!",
                methodToExecute);

            Menu.HaveRunWelcome = true;
            inGameMenu.RunMenu();
            return "0";
        }

        public static string GetPlayerNames(PlayerRole player)
        {
            Console.Clear();
            ConsoleUi.PrintHeader("What is your name?");
            Console.ResetColor();

            Console.SetCursorPosition(10, 6);
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"Player {player}, please write your name.".PadLeft(40).PadRight(60));
            Console.ResetColor();

            Console.SetCursorPosition(10, 8);
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(">");
            Console.ResetColor();
            var userName = Console.ReadLine()?.Trim() ?? player.ToString();
            if (userName.Length > 10) userName = userName.Substring(0, 10);

            return userName;
            
        }
    }
}