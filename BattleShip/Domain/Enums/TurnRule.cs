﻿namespace GameBrain
{
    public enum TurnRule
    {
        CanHaveTurnsInRow,
        CannotHaveTurnsInRow
    }
}