﻿namespace Domain.Enums
{
    public enum CellState
    {
        Free,
        Miss,
        Hit,
        Ship
    }
}