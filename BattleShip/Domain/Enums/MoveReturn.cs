﻿namespace Domain.Enums
{
    public enum MoveReturn
    {
        Miss,
        HitRetry,
        HitNoRetry,
        HitAndSankRetry,
        HitAndSankNoRetry,
        SameSpotTwice,
        Start,
        Null
    }
}