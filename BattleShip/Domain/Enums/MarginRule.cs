﻿namespace Domain.Enums
{
    public enum MarginRule
    {
        CanTouchSides,
        CanTouchCorners,
        CannotTouch
    }
}