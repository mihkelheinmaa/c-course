﻿using System.Collections.Generic;
using Domain.Enums;
using GameBrain;

namespace Domain
{
    public class GameOption
    {
        public int GameOptionId { get; set; }
        
        public int BoardWidth { get; set; }
        public int BoardHeight { get; set; }

        public MarginRule MarginRule { get; set; }
        public TurnRule TurnRule { get; set; }

        public string ShipSizes { get; set; } = default!;
        public string ShipCounts { get; set; } = default!;
        
        public ICollection<Game> Games { get; set; }  = default!;
    }
}