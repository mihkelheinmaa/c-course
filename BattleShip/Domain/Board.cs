﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Board
    {
        public int BoardId { get; set; }

        [MaxLength(128)]
        public string? BoardName { get; set; }
        public string? Winner { get; set; }

        public string Description { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        public bool NextTurnByWho { get; set; }

        public string PlayerABoard { get; set; } = default!;
        public string PlayerBBoard { get; set; } = default!;

        public string PlayerAShips { get; set; } = default!;
        public string PlayerBShips { get; set; } = default!;

        public int ShipsSankByA { get; set; }
        public int ShipsSankByB { get; set; }

        public int GameId { get; set; }
        public Game? Game { get; set; }

        public DateTime CreatedAt { get; set; }


        public override string ToString()
        {
            return $"GAME: {Game?.GameName} | BOARD: {BoardId}) {BoardName}, saved at: {Description}";
        }
    }
}