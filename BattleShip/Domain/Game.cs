﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Game
    {
        public int GameId { get; set; }

        public string Description { get; set; } = DateTime.Now.ToString("yyyy-MM-dd");

        [MaxLength(128)] 
        public string GameName { get; set; } = default!;

        public int GameOptionId { get; set; }
        public GameOption? GameOption { get; set; }

        public int PlayerAId { get; set; }
        public Player PlayerA { get; set; } = default!;
        
        public int PlayerBId { get; set; }
        public Player PlayerB { get; set; } = default!;
        
        public ICollection<Board> Boards { get; set; }  = default!;

        public ICollection<InitiationState> InitiationStates { get; set; }  = default!;

        public DateTime GameCreatedAt { get; set; }

        public override string ToString()
        {
            return $"{GameName}, saved at {Description} | Players: {PlayerA} and {PlayerB}";
        }
    }
}