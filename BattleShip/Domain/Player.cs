﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GameBrain;

namespace Domain
{
    public class Player
    {
        public int PlayerId { get; set; }

        [MaxLength(128)] 
        public string Name { get; set; } = default!;


        public PlayerType PlayerType { get; set; }
        public PlayerRole PlayerRole { get; set; }

        public ICollection<Game> PlayerAFor { get; set; } = default!;
        public ICollection<Game> PlayerBFor { get; set; } = default!;

        public ICollection<InitiationState> InitiationStates { get; set; } = default!;


        public override string ToString()
        {
            return $"{Name}";
        }
    }
}