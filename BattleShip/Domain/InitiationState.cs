﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class InitiationState
    {
        public int InitiationStateId { get; set; }
        
        public string SavedAt { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        public string StateBoard { get; set; } = null!;

        public string PlayerShips { get; set; } = null!;

        public string Progress { get; set; } = "";
        
        public bool IsComplete { get; set; }
        
        [MaxLength(500)]
        public string Resolute { get; set; } = null!;
        
        public int GameId { get; set; }
        public Game? Game { get; set; }

        public int PlayerId { get; set; }
        public Player? Player { get; set; }
    }
}