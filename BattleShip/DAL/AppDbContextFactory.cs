﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DAL
{
    public class AppDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            var dbOptions = new DbContextOptionsBuilder<AppDbContext>();
            dbOptions
                .EnableSensitiveDataLogging()
                .UseSqlServer(@"
                    Server=barrel.itcollege.ee,1533; 
                    User Id=student;
                    Password=Student.Bad.password.0;
                    Database=mihkel.heinmaa_BattleshipTestingDb;
                    MultipleActiveResultSets=true"
                );
            
            return new AppDbContext(dbOptions.Options);
        }
    }
}