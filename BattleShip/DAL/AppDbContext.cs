﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        public DbSet<Player> Players { get; set; } = null!;
        public DbSet<Game> Games { get; set; } = null!;
        public DbSet<GameOption> Settings { get; set; } = null!;
        public DbSet<Board> Boards { get; set; } = null!;

        public DbSet<InitiationState> InitiationStates { get; set; } = null!;
        
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<Game>()
                .HasOne(g => g.PlayerA)
                .WithMany(p => p!.PlayerAFor)
                .HasForeignKey(g => g.PlayerAId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Game>()
                .HasOne(g => g.PlayerB)
                .WithMany(p => p!.PlayerBFor)
                .HasForeignKey(g => g.PlayerBId)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}